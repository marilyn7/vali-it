﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string firstName = textBoxFirstName.Text;
            string lastName = textBoxLastName.Text;
            int age = Convert.ToInt32(textBoxAge.Text);

            string connectionString = "Server=DESKTOP-EA2QDA1; Database=AspNetCore; Trusted_Connection=True";
            //Trusted Connection
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            //MessageBox.Show(connection.State.ToString());
            string sql = "INSERT INTO Student" +
                "(FirstName, LastName, Age)" +
                "VALUES" +
                "(@FirstName, @LastName, @Age)";
            //võtsime algusest "String.Format(" ja lõpust "'{0}', '{1}', {2})", firstName, lastName, age" 
            //ära ja asendasime @-märgiga alguses,
            //et vältida SQL Injectionit
            SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@FirstName", firstName);
            command.Parameters.AddWithValue("@LastName", lastName);
            command.Parameters.AddWithValue("@Age", age);

            int linesChanged = command.ExecuteNonQuery();
            MessageBox.Show("Changes count: " + linesChanged);
        //https://www.connectionstrings.com


        }
    }
}
