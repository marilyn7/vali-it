﻿using System;

namespace WaitAllTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task taskA = DoWork();
            Task taskB = DoMoreWork();

            taskA.Start();
            taskB.Start();

            Task.WaitAny(taskA, taskB);
            Console.WriteLine("All tasks finished!");
            Console.ReadLine();
        }
        public async Task DoWorkAndSum()
        {
            int count = await DoWork();
            Console.WriteLine("{0} jobs done", count);
        }
        static async Task<int> DoWork()
        {

        }

    }
}
