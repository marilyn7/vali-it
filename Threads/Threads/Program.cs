﻿using System;
using System.Threading;

namespace Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread started");
            Thread thread = new Thread(DoWork);
            Console.WriteLine("Main thread continues");
            
            Console.WriteLine("Mis on sinu nimi?");
            var name = Console.ReadLine();
            Console.WriteLine("Tere {0}", name);
            Console.WriteLine("Main thread done");
            Console.ReadLine();
        }
        static void DoWork()
        {
            Console.WriteLine("Starting work");
            for (int i = 0; i < 6; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine(i);
            }
            Console.WriteLine("Work done");
        }
    }
}
