﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singletons
{
        // sealed keelab sellest klassist pärinemise
        // Singleton on selline pattern, mis kindlustab, et seda objekti on alati üks
        public sealed class Singleton
        {
            private static readonly Singleton INSTANCE = new Singleton();
            // Kuna konstruktor on private, siis ei saa sellest klassist otse objekti
            private Singleton() { }

            public static Singleton Instance
            {
                get
                {
                    return INSTANCE;
                }
            }
        public int Age { get; set; }
        }
}
    
