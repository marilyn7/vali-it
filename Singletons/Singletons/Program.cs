﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Singletons
{  
    class Program
    { 
        static void Main(string[] args)
        {
            if (2 == 2)
            {
                Student firstStudent = new Student() { FirstName = "Malle", LastName = "Maasikas" };
                Student secondStudent = new Student() { FirstName = "Jaan", LastName = "Juurikas" };
            }
            Singleton singleton = Singleton.Instance;
            singleton.Age = 20;

            Singleton secondSingleton = Singleton.Instance;
            secondSingleton.Age = 23;
            Console.WriteLine();
        }

}
}
