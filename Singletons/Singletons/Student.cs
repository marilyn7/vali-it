﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singletons
{
    class Student
    {

        // Static property tähendab, et see property on kõigil sellest Student
        // klassist tehtavatel objektidel ühine
        public static int Count { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Student()
        {
            Count++;
        }
    }
}
