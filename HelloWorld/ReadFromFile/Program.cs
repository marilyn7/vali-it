﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadFromFile
{
    class Program
    {
        static void Main(string[] args)
        {   //@ lisamine jutumärkide ette ei nõua topelt kaldkriipsu
            string filePath = @"C:\Users\admin\Documents\GitHub\vali-it\Input.txt";

            //nõuab topelt kaldkriipsu, sest muidu hakkab otsima, mis käsk on \a, \D jne, \n on näiteks reavahetus
            FileStream fileStream = new FileStream("C:\\Users\\admin\\Documents\\GitHub\\vali-it\\Input.txt", FileMode.Open, FileAccess.Read);
            StreamReader streamReader = new StreamReader(fileStream);

            //Iga kord kui ma kutsun uuesti streamReader.ReadLine(), loetakse järgmine rida
            //Kui järgmist rida ei ole, siis streamReader.ReadLine() tagastab nulli
            //1.)
            string line = streamReader.ReadLine();
            int counter = 0;

            while (line != null)
            {
                Console.WriteLine(line);
                line = streamReader.ReadLine();
                counter++;
            }

            //teine variant
                //while ((line = streamReader.ReadLine())!= null)
                //{
                //    Console.WriteLine(line);
                //    line = streamReader.ReadLine();
                //    counter++;
                //}

            streamReader.Close();
            fileStream.Close();
            Console.WriteLine("tekstis oli {0} rida", counter);
            Console.WriteLine();

            //2.)
            Console.WriteLine(File.ReadAllText(filePath)); //siin pannakse streamid jne kinni, sama asi mis oli rida 25-41
            Console.WriteLine();

            //3.)
            string[] lines = File.ReadAllLines(filePath);
            for (int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine(lines[i]);
            }

            Console.ReadLine();
        }
    }
}
