﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booleans
{
    class Program
    {
        static void Main(string[] args)
        {
            /*bool isMinor;

            Console.WriteLine("Mis on sinu vanus?");

            int age = Convert.ToInt32(Console.ReadLine());

            if (age < 18)
            {
                isMinor = true;
                Console.WriteLine("Vastus: Minor");
            }
            else
            {
                isMinor = false;
                Console.WriteLine("Vastus: Not a minor");
            }
            Console.WriteLine(isMinor);

            if (isMinor)
            {
                Console.WriteLine("alaealine");
            }
            else
            {
                Console.WriteLine("Täisealine");
            }*/
            bool haveEaten = false;

            Console.WriteLine("Kas sa täna hommikusööki sõid?");
            Console.WriteLine("Vasta j/e");
            
            if (Console.ReadLine() == "j")
            {
                haveEaten = true;
            }
            Console.WriteLine("Kas sa täna lõunasööki sõid?");
            Console.WriteLine("Vasta j/e");
            

            if (Console.ReadLine() == "j")
            {
                haveEaten = true;
            }

            Console.WriteLine("Kas sa täna õhtusööki sõid?");
            Console.WriteLine("Vasta j/e");
            

            if (Console.ReadLine() == "j")
            {
                haveEaten = true;
            }
            if (haveEaten)
            {
                Console.WriteLine("Oli söömine?");
                Console.WriteLine("Vasta j/e");
                if (Console.ReadLine() != "j")
                {
                    haveEaten = false; //siin saab veel väärtuse ümber pöörata
                }

            }
            if (haveEaten)
            {
                Console.WriteLine("Sa oled täna söönud");
            }
            

            
            else
            {
                Console.WriteLine("Sa ei ole täna söönud");
            }

            bool a = 3 == 3;
            int number = 2;

            bool b = (number < 6 || number > 8) && number != 4;

            if(!a || b)
            {

            }
            Console.ReadLine();


            }
        }
    }

