﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        //static saab otse välja kutsuda ilma, et klassi looks
        //void - kui hiljem pole väärtust vaja kätte saada
        //int  - kui tahan väärtust saada
        static void Main(string[] args)
        {
            PrintHello();
            PrintHello();
            PrintHello();
            PrintText("");

            PrintText("Hello!");
            PrintText("Hello!!");
            PrintText("Hello!!!");
            PrintText("");

            PrintHello(5);
            PrintText("");


            PrintText("Tere!", 7);
            PrintText("");

            PrintText("hei", 5, false);
            PrintText("");
            PrintText("Jah, ühel real");
            PrintText("hei", 5, true);

            Console.ReadLine();
        }

        static void PrintHello()
        {
            Console.WriteLine("Hello");
        }

        //static void PrintText(string text)
        //{
        //    Console.WriteLine(text);
        //}

        static void PrintHello(int howManyTimes)
        {
            for (int i = 0; i < howManyTimes; i++)
            {
                PrintHello();
            }
        }
        //meetod PrintText, kus lisaks tekstile on parameeter, mis ütleb mitu korda printida

        static void PrintText(string text, int howManyTimes)
        {
            for (int i = 0; i < howManyTimes; i++)
            {
                Console.WriteLine(text);
            }
        }

        //veel üks meetod PrintText, mis lisaks tekstile ja korduste arvule omab kolmandat parameetrit, mis määrab ära, 
        //kas need korduvad tektstid prinditakse ühele või mitmele reale


        static void PrintText(string text, int howManyTimes = 1, bool oneLine = false) // need = on default väärtused; neid saab panna ainult paremalt vasakule
            //vahele ei saa jätta. nt ainult text ei saa olla default valuega kui teised pole. ahel peab olema täidetud, järjest
        {

            for (int i = 0; i < howManyTimes; i++)
            {

                if (oneLine)
                {
                    Console.Write(text + " ");
                }


                else
                {
                    Console.WriteLine(text);
                }
            }
        }
    }
}
