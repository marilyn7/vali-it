﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            short a = 1000;
            int b;
            
            b = a;
            //implicit conversion
            //toimub iseenesest teisendus

            Console.WriteLine(a);
            Console.WriteLine(b);

            //short c = b;
            //kuna .NET ei tea, mis on b väärtus, pakub ta teisendust, b on punasega joonitud
            short c = (short)b;
            //Explicit conversion
            //pead ise teisendama (cast)

            //Cast -> teisendus (number ja number)
            //short -> int
            //int -> long

            //Convert -> teisendad eri tüüpide vahel (tekst ja number)
            //string -> int

            Console.WriteLine(c);

            b = 35000;
            c = (short)b;
            Console.WriteLine(c);
            //vastus on -30...
            //kui teisendad eri tüüpide vahel, pead ise veenduma, et teisendus ei läheks üle
            //teatud numbri, short on -31,768 kuni 32,767

            /*sbyte f = 7;
            int = g;
            g = f;

            Console.WriteLine(g);

            f = 7;
            g = f;
            Console.WriteLine(g);*/

            long n = -80000;
            short l = (short)n;

            Console.WriteLine(l);

            //Matemaatilistel tehetel kahe eri numbri tüübi vahel
            //on tulemus alati suurem tüüp
            //välja arvatud tüübid, mis on väiksemad kui int
            // nende tehete tulemus on alati int

            byte d = 244;
            short e = 1000;
            Console.WriteLine(e / d); //4
            Console.WriteLine(d / e); //0
            //vastused ilma komakohtadeta

            ushort i = 22;
            byte j = 3;

            Console.ReadLine();
        }
    }
}
