﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContinueAndBreak
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("hello\t gfg");
                Console.WriteLine("Head\baega!");
                //continue;
                //break;

                Console.WriteLine("Kas soovid jätkata? Vasta j/e");

                if (Console.ReadLine().ToLower() != "j") ;
                {
                    break;
                }


            }
            for (int i = 1; i <= 10; i++)
            {
            
                if (i == 5 || i == 9)  
                { 
                    continue;
                }
                Console.WriteLine(i);
            }Console.ReadLine();
        }

    }
}
