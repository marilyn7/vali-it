﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchCase
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("a");
            Console.WriteLine("b");
            Console.WriteLine("c");
            Console.WriteLine("d");
            Console.WriteLine("e");

            string answer = Console.ReadLine();
            if (answer == "a")
            {
                Console.WriteLine(1);
            }
            if (answer == "b")
            {
                Console.WriteLine(2);
            }
            if (answer == "c")
            {
                Console.WriteLine(3);
            }
            if (answer == "d")
            {
                Console.WriteLine(4);
            }
            else
            {
                Console.WriteLine(5);
            }
            switch (answer)
            {
                case "a":
                case "A":
                case "1":
                case "-1":
                    Console.WriteLine(1);
                            break;
                case "b":
                case "B":
                    Console.WriteLine(2);
                            break;
                case "c":
                            Console.WriteLine(3);
                            break;
                case "d":
                            Console.WriteLine(4);
                            break;
                default:
                            Console.WriteLine(5);
                            break;
            }
            Console.ReadLine();
        }
    }
}
