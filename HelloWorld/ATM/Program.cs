﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM
{
    class Program
    {
        static string pin;
        //see on klassi muutuja, kehtib terve klassi piires ehk kõikides klassi meetodides
        static double balance;

        static void Main(string[] args)
        {
            pin = LoadPin();
            balance = LoadBalance();

            if (!CheckPinCode())
            {
                return;
            }
            Console.WriteLine("Vali tegevus a) Sularaha sissemakse b) Sularaha väljamakse c) Kontojääk d) Katkesta e) Muuda pin");
            string answer = Console.ReadLine();
            switch (answer)

            {
                case "a":
                    {
                        Console.WriteLine("Sularaha sissemakse. Sisesta summa: ");
                        int sum = Convert.ToInt32(Console.ReadLine());
                        balance = balance + sum;
                        Console.WriteLine("Sisestati {0} eurot, kontojääk on {1}", sum, balance);
                        SaveBalance(balance);
                        Console.ReadLine();

                        break;
                    }
                case "b":
                    {
                        Console.WriteLine("Sularaha väljamakse. Sisesta summa: ");
                        int sum = Convert.ToInt32(Console.ReadLine());
                        balance = balance - sum;
                        Console.WriteLine("Väljastati {0} eurot, kontojääk on {1}", sum, balance);
                        SaveBalance(balance);
                        Console.ReadLine();
                        break;

                        //teine variant on teha switchi ja case'dega, kus kasutaja valib summa pakututest

                    }
                case "c":
                    {
                        Console.WriteLine("Kontojääk on: {0}", balance);
                        LoadBalance();
                        Console.ReadLine();
                        break;
                    }
                case "d":
                    {
                        Console.WriteLine("Katkestamiseks vajuta ENTER");//exitProgram
                        Console.ReadLine();
                        break;
                    }
                case "e":
                    {
                        //Console.WriteLine("Muuda pin. Sisesta õige pin: ");
                        //int enterPin = Convert.ToInt32(Console.ReadLine());
                        ChangePin(pin);
                        Console.ReadLine();
                        break;
                    }
                default:
                    Console.WriteLine("Sellist valikut pole, vali uuesti! a/b/c/d/e" +
                    "a) Sularaha sissemakse " +
                    "b) Sularaha väljamakse " +
                    "c) Kontojääk " +
                    "d) Katkesta " +
                    "e) Muuda pin");
                    break;
            }

        }   //SaveBalance(balance) = writetofile

        private static bool CheckPinCode()
        {
            for (int i = 1; i <= 3; i++)
            {
                Console.WriteLine("Tere! Sisesta PIN-kood:");
                string enteredPin = Console.ReadLine();
                if (!IsPinCorrect(enteredPin))
                {
                    Console.WriteLine("vale pin!");
                    if (i < 3)
                    {
                        Console.WriteLine("Proovi uuesti");
                    }
                    else
                    {
                        Console.WriteLine("Kaart on konfiskeeritud");
                        Console.ReadLine();
                        return false; //läheb uuesti Main meetodisse
                    }
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        static string LoadPin()
        {
            string pin = File.ReadAllText(@"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\pin.txt").TrimEnd();
            return pin;
            //return File.File.ReadAllLines("pin.txt")[0] - loeb esimese elemendi
        }

        public static void SavePin(string pin)
        {

            // Salvestatakse pin faili
            File.WriteAllText(@"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\pin.txt", Convert.ToString(pin));
            
            //string[] lines = new string[] { pin };
            //"Peeter ja Jaan ; Juhan, Malle".Split(new string[] { " ja ", ", ", ";" }, StringSplitOptions.None);
            // Salvestatakse pin faili

        }
        public static bool ChangePin(string pin)
        {
            for (int i = 1; i <= 3; i++)
            {
                Console.WriteLine("Sisesta kehtiv PIN-kood");
                string enteredPin = Console.ReadLine();
                if (!IsPinCorrect(enteredPin))
                {
                    if (i < 3)
                    {
                        if (i == 1)
                            Console.WriteLine("Proovi uuesti (2 katset veel");
                        if (i == 2)
                            Console.WriteLine("Proovi uuesti (1 katse veel)");
                    }
                    else
                    {
                        Console.WriteLine("Panid 3x vale PIN-koodi. Kaart on konfiskeeritud!");
                        Console.ReadLine();
                        return false; //läheb uuesti Main meetodisse
                    }
                }
                else
                {
                    Console.WriteLine("Sisesta uus PIN: ");
                    string newPin = Console.ReadLine();
                    Console.WriteLine("Korda uus PIN:");
                    string newPinB = Console.ReadLine();
                    if (newPin == newPinB)
                    {
                        SavePin(newPin);
                        Console.WriteLine("PIN edukalt muudetud!");
                        return true;
                    }

                    return false;
                }
            }
            return false;
        }
        static double LoadBalance()
        {
            double balance;
            //Loeb failist
            string filePath = @"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\balance.txt";
            balance = Convert.ToInt32(File.ReadAllText(filePath));
            return balance;
        }
        static void SaveBalance(double balance)
        {
            File.WriteAllText(@"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\balance.txt", Convert.ToString(balance));
            //salvestatakse faili
        }
        static bool IsPinCorrect(string enteredPin)
        {
            if (enteredPin == pin)
            {
                return true;
            }
            return false;
        }
        //lühem variant
        //return enteredPin == pin;
    }
}
    


