﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleArguments
{
    class Program
    {
        static void Main(string[] args)
        {
               

            // 1. Sõna
            // 2. Mitu korda sõna printida
            //tee lahti command prompt ja cd faili asukoht (bin.debug), siis ConsoleArguments "sõna" "arv"
            //enne vaja teha siin Build

            
            int parameters = args.Length;
            if (parameters != 2)
            {
                Console.WriteLine("Kirjuta 2 asja, sõna ja kordade arv");
            }
            
            else
            {
                string word = args[0];

                int number = Convert.ToInt32(args[1]);

                for (int i = 0; i < number; i++)
                {
                    Console.WriteLine(word);
                }
            }
            
            
            //Console.WriteLine("Kasutus: {0} [word] [number] ", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            
            // 3. Kui kasutaja ei ole 2 parameetrit sisestanud, siis prinditakse kasutajale



            //GetExecutingAssembly()


            Console.ReadLine();
        }
    }
}
