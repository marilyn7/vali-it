﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {

            //küsi kasutajalt arvud, millega soovid tehteid teha
            //eralda arvud tühikuga

            //olenevalt sellest, kas kasutaja sisestas 2, 3 või enam arvu
            //paku kasutajale tehted, mida kasutaja saab nende arvudega teha

            //kui panna 2 arvu
            //Vali tehe
            //a) liida
            //b) lahuta
            //c) korruta
            //d) jaga

            Console.WriteLine("Sisesta arvud, millega soovid tehteid teha: ");

            //string[] elements = Console.ReadLine().Split(' ');
            string numbersString = Console.ReadLine();
            string[] splitNumbers = numbersString.Split(' ');
            int[] numbers = new int[splitNumbers.Length];
            
            
            for (int i = 0; i < splitNumbers.Length; i++)
            {
                numbers[i] = Convert.ToInt32(splitNumbers[i]);
                //Console.WriteLine(numbers[i]);

            }
            Console.WriteLine("Vali tehe: +, -, *, /");
            string mark = Console.ReadLine();

            int sum = 0;
            int minus = 0;
            int multip = 0;
            int divide = 0;

            if (mark == "+")
            {

                for (int i = 0; i < numbers.Length; i++)
                {
                    sum += numbers[i];
                }
                Console.WriteLine("summa on {0}", sum);

            }
            else if (mark == "-")
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    minus = numbers[i]--;
                }
                Console.WriteLine("vahe on on {0} ", minus);

            }
            else if (mark == "*")
            {
                multip = numbers[0] * numbers[1];
                Console.WriteLine("korrutis on {0} ", multip);

            }
            else if (mark == "/")
            {
                 divide = numbers[0] / numbers[1];
                
                Console.WriteLine("jagatis on {0}", divide);
            }

            Console.ReadLine();
            /*int sum = Sum(2, 6);
            Console.WriteLine("Arvude 2 ja 6 summa on {0}", sum);

            int substract = Substract(2, 6);
            Console.WriteLine("Arvude 2 ja 6 vahe on {0}", substract);

            double divide = Divide(2, 6);
            Console.WriteLine("Arvude 2 ja 6 jagatis on {0}", divide);

            int multiply = Multiply(2, 6);
            Console.WriteLine("Arvude 2 ja 6 korrutis on {0}", Multiply(2, 6));

            Console.WriteLine("Arvude summa on {0}", Sum(new int[] { 1, 2, 4, 8, 1, 2, 3 }));//List numbers[] = new numbers[] { 1, 2, -4, 8 };

            Console.ReadLine();
        }

        //muuda korutamise ja liitmise meetodid nii, et toimivad ka 3 numbriga
        static int Sum(int a, int b, int c = 0)
        {
            int sum = a + b + c;
            return sum;
        }

        static int Substract(int a, int b)
        {
            return a - b;
        }

        static double Divide(int a, int b)
        {
            return a / (double)b;
        }

        static int Multiply(int a, int b, int c = 1)
        {
            int multiply = a * b;
            return multiply;
        }

        //loo meetod, mis liidab kõik massivi numbrid kokku
        static int Sum(int[] numbers)
        {
            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                sum += numbers[i];
                //ehk sum = sum + numbers[i]
            }
            return sum;
            */}
        
        

    }
}
