﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Car
    {
        private int year;
        private int speed;
       

        public string Model { get; set; }
        public string Make { get; set; }
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                if(value < 1880)
                {
                    year = 1880;
                }
                else if (value > DateTime.Now.Year)
                {
                    year = DateTime.Now.Year;
                }
                else
                {
                    year = value;
                }
            }
        }
        // private set tähendab, et seda set meetodit saab välja kutsuda ainult klassi enda seest
        public bool IsEngineRunning { get; private set; }

        public int Speed { get; set; }

        public Person Owner { get; set; }
        public Person Driver { get; set; }
        public List<Person> Passengers { get; set; }

        public void StartEngine()
        {
            if (IsEngineRunning)
            {
                Console.WriteLine("Mootor juba töötab");
            }
            else
            {
                Console.WriteLine("Mootor käivitus");
                IsEngineRunning = true;
            }
        }
        //lisage property, kus hoitakse auto hetke kiirust
        //lisage meetod kiirendamiseks, kus saab määrata kui suure kiiruseni kiirendan

        //public int SpeedNow
        //{
        //    get
        //    {
        //        Console.WriteLine("Auto hetke kiirus on {0}", speedNow);
        //        return speedNow;
        //    }
        //    set
        //    {
        //        if (value > 220)
        //        {
        //            speedMax = 220;

        //        }
        //        else
        //        {
        //            speedMax = value;
        //        }
        //    }
        //}

        public void Accelerate (int speed)
        {
            if (!IsEngineRunning)
            {
                Console.WriteLine("Auto mootor ei tööta");

            }
            else if(Speed > speed)
            {
                Console.WriteLine("Ei saa aeglustada suuremale kiirusele");

            }
            else if(speed < 0)
            {
                Console.WriteLine("Auto kiirendas kuni kiiruseni {0}", speed);
            }
            else if(speed > 400)
            {
                Console.WriteLine("Auto nii kiiresti ei sõida");
            }
            else
            {
                Speed = speed;
                Console.WriteLine("Auto aeglustas kuni kiiruseni {0}", speed);

            }
        }
        
        //lisage meetod aeglustamiseks
        //aeglustan

        //lülita mootor välja
        //tee diagnostika (kontrollib õli taset, tosooli taset)
    }
}
