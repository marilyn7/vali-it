﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    enum Gender { Male, Female, Undisclosed
    }
    class Person
    {
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public Person Mother { get; set; }
        public Person Father { get; set; }

        // Staatiline meetod on selline meetod, mille välja kutsumiseks ei ole objekti vaja luua
        //
        static public void PrintFatherName(Person person)
        {
            if(person.Father != null)
            {
                Console.WriteLine(person.Father.FirstName);
                PrintFatherName(person.Father);
                
            }
        }

    }
}
