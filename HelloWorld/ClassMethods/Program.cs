﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            car.Model = "525";
            car.Make = "BMW";
            car.Year = 2000;

            car.StartEngine();
            car.StartEngine();
            if (car.IsEngineRunning)
            {
                Console.WriteLine("Mootor juba töötab");
            }
            else
            {
                Console.WriteLine("Mootor käivitus");
            }

            Person driver = new Person();
            driver.FirstName = "Kalle";
            driver.LastName = "Kaalikas";
            driver.Age = 25;
            driver.Gender = Gender.Male;

            Person owner = new Person()
            {
                FirstName = "Malle",
                LastName = "Maasikas",
                Age = 30,
                Gender = Gender.Female

            };

            car.Driver = driver;
            car.Owner = owner;

            //prindi välja auto omaniku perekonnanimi

            Console.WriteLine("Auto {0} omaniku perekonnanimi on {1}", car.Make, owner.LastName);

            //lisa autosse 3 reisijat ja prindi välja nende vanused


            //var list  = new List <int>
            var passengers = new List<Person>()
            {
                new Person()
                {
                    FirstName = "Priit",
                    LastName = "Pilbas",
                    Age = 58
                },

                new Person()
                {
                    FirstName = "Kadri",
                    LastName = "Kask",
                    Age = 55
                },

                new Person()
                {
                    FirstName = "Teet",
                    LastName = "Tamm",
                    Age = 82
                }
            };
            Console.WriteLine("Reisijate vanused on {0}, {1}, {2}", passengers[0].Age, passengers[1].Age, passengers[2].Age);

            car.Passengers = passengers;
            for (int i = 1; i < car.Passengers.Count; i++)
            {
                Console.WriteLine("{0}. reisija vanus on {1}", i + 1, car.Passengers[i].Age);
            }

            foreach (var passenger in car.Passengers)
            {
                Console.WriteLine(passenger.Age);
            }
            //lisa autojuhile ema ja isa 
            //autojuhi emale ja isale lisa samuti isa
            //ning küsi auto objekti käest mis on selle autojuhi mõlemate vanaisade eesnimed
            
            car.Driver.Father = new Person()
            {
                FirstName = "Tom",
                Father = new Person()
                {
                    FirstName = "Tõnu",
                },
                Mother = owner
            };

            car.Driver.Mother = new Person()
            { 
                FirstName = "Tiina",
                Father = new Person()
                {
                    FirstName = "Paul",
                },
            };

            Console.WriteLine("Autojuhi ema on {0}, isa {1}. {0} isa on {2} ja {1}' isa on {3}", 
                car.Driver.Mother.FirstName, car.Driver.Father.FirstName, car.Driver.Mother.Father.FirstName, car.Driver.Father.Father.FirstName);

            //prindi välja autojuhi kõik meesliini esiisade nimed
            car.Driver.Father.Father.Father = new Person
            {
                FirstName = "John",
                Father = new Person
                {
                    FirstName = "Jake"
                }
            };

            //
            //Tom, Tõnu, John, Jake
            //while (true)
            //{

            //}

            //while (line != null)
            //{
            //    Console.WriteLine(line);
            //    line = streamReader.ReadLine();
            //}



            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
