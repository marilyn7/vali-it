﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VarKeyword
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 3;// int
            var b = 3;// int
            //int määrab ise tüübi
            //täisarvu puhul pannakse väärtuseks int
            //või kui ei mahu, pannakse long
            var c = 3.0;// double
            var d = 3000000000;//
            var e = 5000000000000000000; //ulong, 18 nulli
            var f = b * e;

            var g = "3"; // string
            var h = '3'; // char
            var i = 3.0f; // float
            var j = 3.0d; // double
            var k = 3.0m; // decimal

            var l = Convert.ToInt32("3");
            var m = Convert.ToDouble("3.0");

            var numbers = new[] {1, 2, 3 };
            var words = new[] { "1", "2", "3" };
            var doubles = new int[3];


            FileStream fileStream = new FileStream("minufail.txt");

            Console.WriteLine(a.GetType());
            Console.WriteLine(b.GetType());
            Console.WriteLine(c.GetType());
            Console.WriteLine(d.GetType());


            Console.ReadLine();
        }
    }
}
