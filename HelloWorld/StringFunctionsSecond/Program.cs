﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFunctionsSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kirjuta lause");
            char[] symbols = new char[] { ' ', '_', '-'};
            string sentence = Console.ReadLine();
            //string sentence = "Elas metsas";
            //string sentence = "Elas metsas Mutionu";

            if (sentence.Length == 0)
            {
                Console.WriteLine("Lauses pole sõnu");
            }
            else
            {
                int spaceIndex = sentence.IndexOf(" ");
                if (spaceIndex == -1) //kui ongi ainult 1 sõna ja tühikut ei tule
                {
                    Console.WriteLine("Esimene sõna on {0}", sentence);
                }
                else
                {
                    Console.WriteLine("Esimene sõna on: {0}", sentence.Substring(0, spaceIndex));
                    int secondSpaceIndex = sentence.IndexOf(" ", spaceIndex + 1);
                    if (secondSpaceIndex == -1)
                    {
                        Console.WriteLine("Teine sõna on: {0}", sentence.Substring(spaceIndex + 1));
                        //substring 1 parameetriga on parameetrist kuni lõpuni
                    }
                    else
                    {
                        Console.WriteLine("Teine sõna on: {0}", sentence.Substring(spaceIndex + 1, secondSpaceIndex - spaceIndex - 1));
                    }
                }
            }

            //string split
            //kui string.split ei leia seda sümbolit, millega ma tahan tükeldada stringi, 
            //siis ta tagastab massiivi ühe endiga ja see üks element ongi esialgne terve string
            string[] splitWords = sentence.Split(' ');
            if(splitWords.Length == 0)
            {
                Console.WriteLine("Lauses pole sõnu");
            }
            else 
            {
                Console.WriteLine("Esimene sõna on {0}", splitWords[0]);
                if(splitWords.Length > 1)
                {
                    Console.WriteLine("Teine sõna on {0}", splitWords[1]);
                }
                else
                {
                    Console.WriteLine("Teine sõna on puudu");
                }
   
            }
            //for (int i = 0; i < splitWords.Length; i++)
            //{
            //    //int wordIndex = sentence.IndexOf();
            //    Console.WriteLine("{0}. {1}", wordIndex, splitWords[i]);
            //}

            string word = "raudtee";

            Console.WriteLine(word.Replace('r', 'R'));
            Console.WriteLine(word.Replace("raud", "tee"));
            Console.WriteLine("Mina, Pets".Replace(",", "").Replace(" ", ""));

            string someString = "põder jooksis öösel üle ääre";
            Console.WriteLine(someString
                .Replace('ä', 'a')
                .Replace('ö', 'o')
                .Replace('ü', 'u')
                .Replace('õ', 'o')
                .Replace('Ä', 'A')
           
            );
            Console.WriteLine("Sisesta oma kaal");
            //79,54
            //79.54

            double weight = Convert.ToDouble(Console.ReadLine().Replace(",", "."), new CultureInfo("en-US"));
            Console.WriteLine("Kaal on {0:0.00}", weight); //{0:0.00} see näitab mitu komakohta kuvab, ümardab selliseks, et oleks 2 kohta peale koma
            //{0:0.00}
            //{0:0.000}


            
            Console.WriteLine("Sisesta oma tel nr");
            // +37259000000
            // 59 00 0000 0

            long number = Convert.ToInt32(Console.ReadLine().Replace(".", "").Replace(" ", "").Replace("+372", ""));
            Console.WriteLine("Number on {0}", number);//teeb järjest numbrid

            //arvenumbrite genereerimiseks
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Date: {0}{1:00}{2}, Invoice: {3:0000000}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, i);
            }

            Console.ReadLine();
            
        }
    }
}
