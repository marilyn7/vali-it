﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chars
{
    class Program
    {
        static void Main(string[] args)
        {
            char letter = 'a'; //ühekordsed jutumärgid
            Console.WriteLine(letter);
            
            Console.WriteLine('t' + 'e' + 'r' + 'e');//chars
            Console.WriteLine("t" + "e" + "r" + "e");//strings

            Console.WriteLine("Sisesta oma nime esimene täht: ");
            char a = Convert.ToChar(Console.ReadLine());

            if(a == 'M') //võrdlen chari
            {
                Console.WriteLine("Esimene täht oli õige");
            }
            else
            {
                Console.WriteLine("Vale täht");
            }
            char[] letters = new char[5];
            letters[0] = 'a';
            letters[1] = 'c';
            letters[2] = '?';
            letters[3] = '\n';
            letters[4] = '3';

            for (int i = 0; i < letters.Length; i++)
            {
                Console.WriteLine(letters[i]);
            }

            Console.WriteLine(new string(letters));
            char[] word = new char[] {'m', 'a','j', 'a' };

            Console.WriteLine(string.Join(" ", word));
            Console.ReadLine();
        }
    }
}

