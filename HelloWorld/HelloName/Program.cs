﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloName
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            /* string on muutuja, kus saame hoida teksti
            //name on muutuja nimi
            //defineerime muutuja tüübist string (teksti sisaldav muutuja tüüp) nime 'name'
            name = "Marilyn";
            //anname muutujale 'name' väärtuse "Marilyn"
            //muutujad on väikse algustähega
            string greeting = "Tere, \"" + name + "\"!";
            //  Escape \ laseb kasutada stringi sees sümbolit "", võtab järgmise sümboli
            // Escape \\ laseb kasutada kaldkriipsu sümbolit teksti sees
            // Nii on Marilyn jutumärkides*/

            //Console.WriteLine(greeting);

            /*string substr = greeting.Substring(10);
            võtab alates 10. tähest ja prindib
            Console.WriteLine(substr);*/
            string greeting;
            
            Console.WriteLine("Mis su eesnimi on?");
            string fName = Console.ReadLine();
            Console.WriteLine("Mis su perekonnanimi on?");
            string sName = Console.ReadLine();
            greeting = "Tere, " + fName + " " + sName + "!";
            Console.WriteLine(greeting);
            Console.ReadLine(); 
            //aken jääb lahti ja ootab enterit või sisend+enterit
        
        }
    }
}
