﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {

            for (int i = 1; i < 11; i++)//üks rida, kokku tuleb 10 rida
            {

                for (int j = 1; j < 11; j++)//ühe rea iga element
                {

                    Console.Write("{0,5}", j * i);//see näitab, mitu sümbolikohta üks number saab
                    
                }
                Console.WriteLine();

            }
            Console.ReadLine();

        }
    }
}


