﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //Lõpmatu while tsükkel
            //while(true)
            //{
            //    Console.Write("Hello");
            //}
            bool isGameOver = false;

            while (!isGameOver)
            {
                Random random = new Random(); //juhusliku arvu generaatori objekti loomine

                int correctNumber = random.Next(1, 6); //arvud 1,2,3,4,5
                int number = -999999;
                bool isFirstTry = true;
                //kui kasutaja pakub midagi muud, kui 1 ja 5 vahel numbri
                //kirjuta kasutajale, proovi uuesti, sest pakutud number ei olnud 1 ja 5 vahel

                while (number != correctNumber)//korratakse nii kaua, kuni sulgudes avaldis on true ehk pakutud number ei võrdu õige nriga
                {

                    if (!isFirstTry && (number < 1 || number > 5)) //esimene kord hüppab üle
                    {
                        Console.WriteLine("Proovi uuesti, sest pakutud number ei olnud 1 ja 5 vahel");
                    }
                    else
                    {
                        Console.WriteLine("Arva ära number 1 ja 5 vahel");
                        isFirstTry = false; //edaspidi alati false, esimene kord proovida sai läbi
                    }
                    number = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Õige number oligi {0}! ", number);
                Console.WriteLine("Kas soovid uuesti arvata? Vasta j/e");
                string answer = Console.ReadLine();
                if(answer.ToLower() != "j") //kui pole j, viskab kohe mängu kinni, teisendab enne võrdlemist väikseks täheks (J oleks ka õige)
                {
                    isGameOver = true;
                }
            }


        }
    }
}
    



