﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDictionary
{
    class Program
    {
        static void Main(string[] args)
        {

            //sõnapaarid
            //"auto" - "car"
            //"maja" - "house"
            //"kass" - "cat"
            //"koer" - "dog"
            
            Dictionary<string, string> dictionaryEn = new Dictionary<string, string>();
            dictionaryEn.Add("auto", "car");
            dictionaryEn.Add("maja", "house");
            dictionaryEn.Add("kass", "cat");
            dictionaryEn.Add("koer", "dog");

            Console.WriteLine("Keys: ");

            foreach (string item in dictionaryEn.Keys)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Values: ");

            foreach (string item in dictionaryEn.Values)
            {
                Console.WriteLine(item);
            }

            Dictionary<string, string> dictionaryDe = new Dictionary<string, string>();
            dictionaryDe.Add("auto", "Auto");
            dictionaryDe.Add("maja", "Haus");
            dictionaryDe.Add("kass", "Katze");
            dictionaryDe.Add("koer", "Hund");
            
            Console.WriteLine("Mis keelde soovid sõna tõlkida? inglise/saksa");
            string language = Console.ReadLine();
            Console.WriteLine("Mis sõna soovid tõlkida?");
            Console.WriteLine("Tunnen selliseid sõnu: {0}", string.Join(", ", dictionaryEn.Keys));
            string word = Console.ReadLine();

            if (language == "inglise")
            {
                Console.WriteLine("'{0}' {1} keeles on '{2}'", word, language, dictionaryEn[word]);
            }

            else if(language == "saksa")
            {
                Console.WriteLine("'{0}' {1} keeles on '{2}'", word, language, dictionaryDe[word]);
            }
            else
            {
                Console.WriteLine("Sellist keelt ma ei oska!");
            }

            Dictionary<string, string> countryCodes = new Dictionary<string, string>();
            countryCodes.Add("EST", "Estonia");
            countryCodes.Add("EE", "Estonia");
            countryCodes.Add("GER", "Germany");
            countryCodes.Add("USA", "United States of America");

            //Asenda "United States of America" => "United States"
            countryCodes["USA"] = "United States";

            //küsi kasutajalt riigikood ja sellele vastav riigi nimetus
            //Kui selline riigikood on olemas, siis asenda riik
            //Kui sellist riigikoodi veel ei olnud, siis lisa

            Console.WriteLine("Palun riigikoodi: ");
            string code = Console.ReadLine();
            


            if (countryCodes.ContainsKey(code))
            {
                Console.WriteLine("See riigikood on riik: " + countryCodes[code]);
                //countryCodes[countrycode] = countrycode;
            }
            else
            {
                Console.WriteLine("Sellist pole, mis see tähendab?");
                string country = Console.ReadLine();
                countryCodes.Add(code, country);
            }

            foreach (var item in countryCodes) //siin prindin välja sõnapaarid
            {
                Console.WriteLine("{0} => {1}", item.Key, item.Value);
            }
            Console.ReadLine();





        }
    }
}
