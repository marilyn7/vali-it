﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElseIf
{
    class Program
    {
        static void Main(string[] args)
        {
            //küsi kasutajalt arv1
            //küsi kasutajalt arv2
            //2 arvu on võrdsed, prindi tekst arvud on võrdsed
            //kui esimene arv on suurem kui 2., prindi, et esimene on suurem
            //muul juhul prindi, et teine on suurem

            Console.WriteLine("Kirjuta üks number:");
            int esimeneArv = Convert.ToInt32((Console.ReadLine()));

            Console.WriteLine("Kirjuta veel üks number:");
            int teineArv = Convert.ToInt32(Console.ReadLine());

            if (esimeneArv == teineArv)
            {
               Console.WriteLine("Need arvud on võrdsed");
            }
            
            else if (esimeneArv != 0 && teineArv !=0 && esimeneArv == teineArv)
            {
                Console.WriteLine("Arvud on nullid");
            }
            else if(esimeneArv > teineArv)
            {
                Console.WriteLine("Esimene arv on suurem");
              
            }
            else 
            //NB! Else kehtib alati ainult omale eelnenud IFi kohta
             {
                Console.WriteLine("Teine arv on suurem");
            }
            Console.ReadLine();
        }
    }
}
