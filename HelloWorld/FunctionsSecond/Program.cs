﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionsSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "elas metsas mutionu";
            //kasutades meetodeid IndexOf, Substring
            //prindi ekraanile lause esimene sõna

            Console.WriteLine("Esimene a-täht on " + (sentence.IndexOf('a') + 1) + ". täht lauses");
            string firstWord = sentence.Substring(0, sentence.IndexOf(" ")); //otsib üles tühiku asukoha
            Console.WriteLine("Essa sõna: " + firstWord);

                //või

            int spaceIndex = sentence.IndexOf(" ");
            Console.WriteLine("Essa sõna: " + sentence.Substring(0, spaceIndex));
            //Console.WriteLine(spaceIndex);

            //teine sõna

            string secondWord = sentence.Substring(sentence.IndexOf(" ") + 1, (sentence.IndexOf(" ") + 2));
            Console.WriteLine("Teine sõna: " + secondWord);
            

                //või
            int secondSpaceIndex = sentence.IndexOf(" ", spaceIndex + 1);
            //Console.WriteLine(secondSpaceIndex); //kus on teine space
            Console.WriteLine("Teine sõna:" + "'" + sentence.Substring(spaceIndex + 1, secondSpaceIndex - spaceIndex - 1) + "'");


            ////kui seda asja pole lauses
            //string word = "sõna";
            //int spacIndex = word.IndexOf(" ");
            //Console.WriteLine(word.Substring(0, spacIndex));

         


            Console.ReadLine();

        }
    }
}
