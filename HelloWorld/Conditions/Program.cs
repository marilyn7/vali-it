﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-US");
            // koma ei arvesta, arvestab teda tuhandike eraldajana
            // komakoht on ainult punkt

            Console.WriteLine("Sisesta oma lemmiknumber: ");
            string answer = Console.ReadLine();
            answer = answer.Replace(",", ".");
            //asendab tekstis kõik komad punktidega

            //esimene variant koos eelmise reaga : int a = Convert.ToInt32(answer);
            //teine variant  - int a = Convert.ToInt32(Console.ReadLine());

            /*Console.WriteLine(CultureInfo.CurrentCulture.DisplayName);
            double a = Convert.ToDouble(answer, CultureInfo.InvariantCulture);*/

            double a;

            
            if (CultureInfo.CurrentCulture.Name == "et-EE")
            {
                //kasuta , reaalarvus
                a = Convert.ToDouble(answer);
            }
            else
            {
                //kasuta . reaalarvus
                //lisaparameeter konverteerimise keelena
                a = Convert.ToDouble(answer, new CultureInfo("en-US"));

            }
            Console.WriteLine(a);
            //siin on vaja teha converti, et kasutaja sisestus muuta intiks
            if (a == 3)
            {
                Console.WriteLine("Arv on võrdne kolmega");
            }

            if (a != 3)
            {
                Console.WriteLine("Arv ei ole võrdne kolmega");
            }

            if (a > 2)
            {
                Console.WriteLine("Arv on suurem kui 2");
            }

            if (a < 6)
            {
                Console.WriteLine("Arv on väiksem kui 6");
            }

            if(a >= 5)
            {
                Console.WriteLine("Arv on suurem või võrdne 5ga");
            }
            
            //|| -> VÕI 
            //&& -> JA

            
            if(2 < a && a < 8) //Kui arv on 2 ja 8 vahel
            {
                Console.WriteLine("Arv on 2 ja 8 vahel");
            }
            
            if (2 > a || a > 8) //Kui arv on väiksem kui 2 või suurem kui 8
            {
                Console.WriteLine("Arv on väiksem kui 2 või suurem kui 8");
            }
            if ((2 < a && a < 8) || (5 < a && a < 8) || a > 10) 
                //Kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
                //&& jaatus on || või'st alati eespool, kui mul ka nii vaja, pole sulge vaja
                //aga hea on panna sulud
            {
                Console.WriteLine("Arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10");
            }

        
            {

            }
            Console.ReadLine();
        }
    }
}

