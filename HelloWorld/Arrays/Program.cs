﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Loend või massiiv. Mingi hulk näiteks numbreid, sõnu vms
namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //luuakse täisarvude massiiv numbers, millesse mahub 5 täisarvu
            //Loomise hetkel pean määrama kui suure (mitu numbrit näitab) massiivi teen
            int[] numbers = new int[5];
            //Massiivi indeksid algavad 0st
            //Massiivi maksimum elementide arv on alati 1 võrra väiksem kui massiivi maksimum element (-1), sel juhul 4
            numbers[0] = 1;
            numbers[1] = 2;
            numbers[2] = -1;
            numbers[3] = -11;
            numbers[4] = 12;

            //1. printi õigetpidi

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(numbers[i]);
                


            }Console.ReadLine();
            //2.prindi tagurpidi
            for (int i = 4; i > 0; i--)
            {
                Console.WriteLine(numbers[i]);
                

            }Console.ReadLine();


            //3. printi kõik positiivsed numbrid

            for (int i = 0; i < 5; i++)
            {
                if (numbers[i] > 0)
                {
                    Console.WriteLine(numbers[i]);
                    

                }
            }Console.ReadLine();

            //4. loo teine massiiv 3le numbrile ja pane sinna esimese massiivi 3 esimest numbrit. prindi teise massiivi numbrid ekraanile

            int[] numbersB = new int[3];
            numbersB = numbers;
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(numbersB[i]);
            }
            
            // Console.WriteLine("Õnnenumber on " + numbers[3]);
            Console.ReadLine();

            //5. loo teine massiiv 3le numbrile ja pane sinna 
            //3 nrt tagantpoolt alates

                //1)
                int[] numbersC = new int[3];
                numbersC = numbers;
                //numbersC[0] = numbers[4];  -mis mõeldud on
                for (int i = 0; i < numbersC.Length; i++)
                {
                    numbersC[i] = numbers[numbersC.Length - 1 - i];
                }
                //2)
                int a = 4;
                for (int i = 0; i < numbersC.Length; i++)
                {
                    numbersC[i] = numbers[a];
                    a--;
                }

                //3))
                for (int i = 4; i > 1; i--)
                {
                    Console.WriteLine(numbersC[i]);
                }
                Console.ReadLine();

        }
    }
}
