﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace For
{
    class Program
    {
        static void Main(string[] args)
        {
            //For tsükkel on selline tsükkel, kus korduste arv on teada
            //Lõpmatu tsükkel
            //for( ; ; )
            //{
            //    Console.WriteLine("Hello");
            //}

            for (int i = 0; i < 3; i++)
            //1)  int i = 0; -> siin saab luua muutujaid ja neid algväärtustada
            //    luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma

            //2)  i < 7 -> tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks

            //3)  i++ -> tegevus, mida ma tahan iga tsükli korduse lõpus korrata
            //    i++ o sama, mis i = i + 1, ehk iseenda suurendamise 1 võrra
            {

                Console.WriteLine("Hello");
            }
            for (int i = 0; i < 5; i++)

                Console.WriteLine(i);

            //prindi arvud 50-100

            for (int i = 50; i < 101; i++)
            {
                Console.WriteLine(i);
            }

            //prindi arvud -20 kuni 10ni ja 20st 40ni
            for (int i = -20; i <= 40; i++)
            {
                /*Console.WriteLine(i);

                if(i <= 10 || i >= 20)
                {
                    Console.WriteLine(j);
                }*/

                if(i == 11)
                {
                    i = 20;
                }
                Console.WriteLine(i);
                
            }
            //prindi arvud 100 kuni 20
            for (int i = 100; i > 19; i--)
            {
                Console.WriteLine(i);
            }
            //prindi üle ühe arvud 1 kuni 100 (1,3,5,7...99)
            for (int i = 0; i <= 99; i += 2)  //i = i + 2  sest i++ on i = i + 1
                //i = i -4 => i -+ 4
                //i = i * 2 => i *= 2
                //i = i / 2 => i /= 2
            {
                Console.WriteLine(i+1);
            }
            Console.ReadLine();
        }
    }
}
