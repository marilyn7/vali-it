﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Konstruktor on eriline meetod, mis käivitatakse objekti loomisel
// Kasutatakse näiteks mingite muutujale algväärtustamiseks
namespace Constructors
{
    class Program
    {
        static void Main(string[] args)
        {
            // Dog() tähendabki parameetrita konstruktori välja kutsumist
            Dog tax = new Dog();
            Dog buldog = new Dog();
            Dog rotveiler = new Dog("Naki", 2);
            Console.WriteLine("Taxi nimi on {0} ja vanus on {1}", tax.Name, tax.Age);
            Console.WriteLine("Rotveileri nimi on {0} ja vanus on {1}", rotveiler.Name, rotveiler.Age);

            Console.ReadLine();
        }
    }
}
