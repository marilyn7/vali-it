﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableScope
{
    class Program
    {
        //Scope - loogsulgude piir
        //kõik muutujad, mis ma defineerin, kehtivad/elavad {} piires
        static void Main(string[] args)
        {
            int a = 4;//kui if blokkides on sellel a'l erinevad väärtused, siis peab ta enne väljaspool ära defineerima (kas on int)

            Increment(a);
            Console.WriteLine("Value of a is {0}", a);

            string sentence = "Mis teed";
            AddQuestionMark(sentence);

            Console.WriteLine("Value of sentence is {0}", sentence);

            int[] numbers = new int[] { 2, 4, 23, -11, 12 };
            PrintNumbers(numbers);
            Increment(numbers);
            Console.WriteLine();

            PrintNumbers(numbers);
            Console.WriteLine();

            int b = 3;
            int c = b;
            
            b = 7;
            Console.WriteLine("b on " + b);

            int[] secondIntegers = numbers;
            secondIntegers[0] = -7;
            Console.WriteLine(secondIntegers[0]);

            string firstWord = "raud";
            string secondWord = firstWord;
            firstWord = firstWord + "tee";
            Console.WriteLine(secondWord);
            Console.WriteLine(firstWord);

            //muudan ühte, muutub teine
            List<int> numbersA = new List<int>() { 2, -4, 0 };
            List<int> secondNumbers = numbersA;

            numbersA.RemoveAt(2);

            Console.WriteLine();

            for (int i = 0; i < secondNumbers.Count; i++)
            {
                Console.WriteLine(secondNumbers[i]);
            }
 

            Console.ReadLine();
        }
            //for (int i = 1; i < 9; i++)
            //{
            //    for (int j = 1; j < 9; j++)
            //    {
            //        for (int k = 1; k < 9; k++)
            //        {
            //            for (int l = 1; l < 9; l++)
            //            {
            //                Console.WriteLine("{0} {1} {2} {3}", i, j, k, l );
            //            }
            //        }
            //    }
            //}
        
        //kui meetodile anda parameetriks kaasa lihttüüpi 'Value Type' muutuja väärtus, siis tegelikult tehakse uus muutuja ja pannakse sinna sama väärtus

        //Value Type - väärtusmuutuja; int, char, string, double, float, bool, short, long jne
        //Reference Type - viitmuutuja; massiivid, listid, dictionary, kõik klassid, queue, stack
        //Kui meetodile anda parameetriks kaasa Reference Type muutuja, siis antakse kaasa tegelikult sama muutuja
        //(uus muutuja, mis viitab samale mäluaadressile)

        static void Increment(int number)
        {
            number++;
            Console.WriteLine("New number is {0}", number);
        }

        static void Increment(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i]++; //siin liidab igale ühikule arrays +1
            }
        }

        static void PrintNumbers(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }
        }
        static void AddQuestionMark(string sentence)
        {
            sentence = sentence + "?";
            Console.WriteLine("New sentence is {0}", sentence);
        }
    }
}
