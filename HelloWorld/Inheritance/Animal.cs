﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Animal : Object //kõik klassid pärinevad asjast "object"
        //vaikimisi on kõigil klassidel pärinemine olemas
    {
        public string Breed { get; set; }
        public virtual string Name { get; set; }
        public string Color { get; set; }
        public int Age { get; set; }

        //Virtual tähendab, et seda meetodit saavad pärinevad klassid üle kirjutada
        public virtual void Eat()
        {
            Console.WriteLine("Söön toitu");
        }

        public virtual void PrintInfo()
        {
            var type = GetType();
            Console.WriteLine();
            Console.WriteLine("{0} Info:", GetType().Name);
            Console.WriteLine();
            Console.WriteLine("nimi: {0}", Name);
            Console.WriteLine("tõug {0}", Breed);
            Console.WriteLine("vanus {0}", Age);
            Console.WriteLine("värvus {0}", Color);

        }
        public string GetInfo()
        {
            return String.Format("Liik on {0}, nimi on {1}, Tõug on {2}", GetType().Name, Name, Breed);
        }
    }
}
