﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Tiger : Carnivore
    {
        public void Speak()
        {
            Console.WriteLine("Roar!");
        }
    }
}
