﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Dog : Pet
    {
        private int yearOfBirth;

        public bool IsChained { get; set; }

        // see on parameetriteta konstruktor
        // Vaikimisi on alati igal klassil üks tühi parameetriteta konstruktor, senikaua kui me uue konstruktori loome
        // Kui ma loon näiteks uue 2 parameetriga konstruktori, siis kustutatakse vaikimisi nähtamatu parameetritega konstruktor ära

        public Dog() 
        {
            File.AppendAllText("dog.txt", "Loodi koer: " + DateTime.Now + "\r\n");
            Console.WriteLine("Loodi Dog objekt");
            // Vaikimisi kõik koerad on Marid
            Name = "Mari";

            yearOfBirth = DateTime.Now.Year;

            // Vaikimisi kõik koerad on 1-aastased
            Age = 1;
            // Vaikimisi kõik koerad on ketis
            IsChained = true;

        }
        public Dog(string name, int age)
        {
            File.AppendAllText("dog.txt", "Loodi koer: " + DateTime.Now + "\r\n");

            Name = name;
            Age = age;
        }

        public void DigHole()
        {
            Console.WriteLine("Kaevan auku");
        }
       
        public void Bark(string language)
        {
            if (language == "ru")
            {
                Console.WriteLine("gaf gaf");
            }
            else if (language == "en")
            {
                Console.WriteLine("woof woof");
            }
            else
            {
                Console.WriteLine("auh ah");
            }
        }
    }
}
