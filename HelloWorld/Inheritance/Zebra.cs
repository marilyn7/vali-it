﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Zebra : Herbivore
    {
        public void RunFromTiger()
        {
            Console.WriteLine("Põgenen!");
        }
    }
}
