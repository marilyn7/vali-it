﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Pet : DomesticAnimal
    {
       public string FavouriteToy { set; get; }
       public override void PrintInfo()
        {
            base.PrintInfo();
            if (FavouriteToy != null)
            {
                Console.WriteLine("Lemmik mänguasi on {0}", FavouriteToy);
            }
        }
    }
}
