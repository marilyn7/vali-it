﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program
    {
      

        static void Main(string[] args)
        {
            Cat persian = new Cat();
            persian.Age = 2;
            persian.LivesRemaining = 4;
            persian.Breed = "Persian";
            persian.Name = "Pärli";
            persian.Color = "Brown";
            persian.CatchMouse();
            persian.Eat();
            persian.OwnerName = "Mari";
            persian.PrintInfo();

            Cat bengali = new Cat();
            bengali.Age = 2;
            bengali.LivesRemaining = 8;
            bengali.Breed = "Bengali";
            bengali.Name = "Kiisu";
            bengali.Color = "Brown";
            bengali.CatchMouse();
            bengali.Eat();
            Console.WriteLine(bengali);
            bengali.PrintInfo();

            Dog lab = new Dog();
            lab.Age = 5;
            lab.Breed = "labrador";
            lab.Name = "Doggo";
            lab.Color = "Yellow";
            lab.PrintInfo();
                        lab.DigHole();
            lab.Eat();

            Zebra zebra = new Zebra();
            zebra.Name = "Sebrake";
            zebra.Color = "Black&White";
            zebra.Breed = "African Maneless";
            zebra.Age = 4;
            zebra.PrintInfo();
            zebra.RunFromTiger();

            Tiger tiger = new Tiger();
            tiger.Name = "Tiigerviiger";
            tiger.Color = "Orange";
            tiger.Breed = "Bengali";
            tiger.Age = 8;
            tiger.PrintInfo();
            Console.WriteLine(tiger);
            tiger.Speak();
            tiger.Hunt(zebra);
            Console.WriteLine("Viimane saak {0}", tiger.LastHuntedAnimal.Name);

            Sheep sheep = new Sheep();
            sheep.Name = "Lambake";
            sheep.Color = "White";
            sheep.Breed = "Regular";
            sheep.Age = 5;
            sheep.PrintInfo();
            Console.WriteLine(sheep);
            sheep.Speak();

            Console.WriteLine("Tiiger");
            tiger.Eat();

            Console.WriteLine("Sebra");
            zebra.Eat();

            Console.WriteLine("Bengali kass");
            bengali.Eat();

            Console.ReadLine();
        }
    }
}
