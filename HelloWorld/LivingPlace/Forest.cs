﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace LivingPlace
{
    public class Forest : ILivingPlace
    {
        private List<Animal> animals = new List<Animal>();
        private Dictionary<string, int> animalCounts = new Dictionary<string, int>();

        public int MaxAnimalCount
        {
            get
            {
                return int.MaxValue;
            }
        }
        public void AddAnimal(Animal animal)
        {
            animals.Add(animal);
            Console.WriteLine("{0} lisati farmi", animal.GetType().Name);
            // Uue looma lisamine
            if (!animalCounts.ContainsKey(animal.GetType().Name))
            {
                animalCounts.Add(animal.GetType().Name, 1);
                return;
            }
            animalCounts[animal.GetType().Name]++;
        }

        public int GetAnimalCount(string animalType)
        {
            if (animalCounts.ContainsKey(animalType))
            {
                return animalCounts[animalType];
            }
            Console.WriteLine("Looma {0} ei leitud", animalType);
            return 0;
        }

        public void PrintAnimals()
        {
            foreach (var animalCount in animalCounts)
            {
                Console.WriteLine("{0} {1}", animalCount.Key, animalCount.Value);
            }
        }

        public void RemoveAnimal(string animalType)
        {
            if (!animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine("Looma {0} ei leitud", animalType);
                return;
            }

            for (int i = 0; i < animals.Count; i++)
            {
                if (animals[i].GetType().Name == animalType)
                {
                    animals.Remove(animals[i]);

                    if (animalCounts[animalType] == 1) // kui count on 1, järelikult oli viimane
                    {
                        animalCounts.Remove(animalType); // remove key järgi (animal type)
                    }
                    else
                    {
                        animalCounts[animalType]--;
                        break;


                    }
                }
            }
        }
    }
}
