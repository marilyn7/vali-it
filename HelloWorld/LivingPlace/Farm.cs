﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace LivingPlace
{
    public class Farm : ILivingPlace
    {
        private Dictionary<string, int> animalCounts = new Dictionary<string, int>();

        private List<FarmAnimal> animals = new List<FarmAnimal>(); //<FarmAnimal> näitab, et saab ainult farmiloomi lisada (need kes inheritance all

        public int MaxAnimalCount { get; private set; } //private set ehk väljaspoolt seda muuta ei saa 

        public Farm(int count = 25) //nii saab lihtsalt muuta, kui max number peaks muutuma
        {
            MaxAnimalCount = count;
        }
        // 1. lisada loomi
        public void AddAnimal(Animal animal)
        {
            if (!(animal is FarmAnimal))
            {
                Console.WriteLine("Farmis saavad elada ainult farmiloomad");
                return;
            }
            if (animals.Count >= MaxAnimalCount)
            {
                Console.WriteLine("Farmis on juba maksimum kogus loomi");
                return;
            }
            if (!animalCounts.ContainsKey(animal.GetType().Name)
                && animalCounts.Count >= 4)
            {
                Console.WriteLine("Farmis on juba 4 erinevat looma");
                return;
            }
            if (animalCounts.ContainsKey(animal.GetType().Name)
                && animalCounts[animal.GetType().Name] >= 5)
            {
                Console.WriteLine("Farmis on juba 5 {0}", animal.GetType().Name);
                return;
            }

            animals.Add((FarmAnimal)animal);
            Console.WriteLine("{0} lisati farmi", animal.GetType().Name);
            // Uue looma lisamine
            // animalCounts.Count näitab alati mitu erinevat looma mul on
            if (!animalCounts.ContainsKey(animal.GetType().Name))
            {
                animalCounts.Add(animal.GetType().Name, 1);
                return;
            }
            animalCounts[animal.GetType().Name]++;
            //wordCounts[word] = wordCounts[word] + 1;
            //wordCounts[word] += 1;

        }
        // eelmine variant, aga läheb liiga keeruliseks kui if on if sees. siis tuleks if tingimus ümber muuta ja invert if
        ////{
        ////    // Uue looma lisamine
        ////    if (animals.Count < MaxAnimalCount && !animalCounts.ContainsKey(animal.GetType().Name)  // ehk kui on veel 4 või vähem, siis saab LISADA
        ////        && animalCounts.Count < 4) // iga kord enne lisamist vaatab palju on juba (alla 15)
        ////    {
        ////        animals.Add(animal);
        ////        animalCounts.Add(animal.GetType().Name, 1);
        ////    }
        ////    else if (animalCounts.ContainsKey(animal.GetType().Name) && animalCounts[animal.GetType().Name] < 5) //kui ei olnud sellist keyd ja 
        ////                                                                                         //"animalCounts.Count" kui oli vähem kui 4 key-value paari
        ////    {
        ////        animals.Add(animal);
        ////        animalCounts[animal.GetType().Name]++; // siis lisa selline loom
        ////    }
        ////    else //kui oli selline key
        ////    {
        ////        Console.WriteLine("Loom ei mahu farmi"); 
        ////        //siis suurenda, count suureneb
        ////    }
        ////}

        // 2. küsida mis loomad on
        // prindib looma tüübi koos arvuga
        // Cow 4
        // Pig 2

        public void PrintAnimals()
            {
                foreach (var animal in animalCounts)
                {
                Console.WriteLine("{0} {1}", animal.Key, animal.Value);
                }
            }
        // 3. küsida konkreetse looma arvu  
        public int GetAnimalCount(string animalType)
        {
            if (animalCounts.ContainsKey(animalType))
            {
                return animalCounts[animalType];
            }
            Console.WriteLine("Looma {0} ei leitud", animalType);
            return 0;
        }
     
    // 4. eemaldada loomi
        public void RemoveAnimal(string animalType)
        {
            if (!animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine("Looma {0} ei leitud", animalType); // kui ei leitud sellist looma, 
                // siis kohe returnib ja ei lähegi järgmisesse for blokki. parem kui kõige lõppu panna else ja "looma ei leitud"
                return;
            }
            //animals.Remove(FarmAnimal animal) - removida saab 1 liigi kaupa
            //animals.RemoveAt(int index) - või indexi järgi listis

            for (int i = 0; i < animals.Count; i++)
            {
                if (animals[i].GetType().Name == animalType)
                {
                    animals.Remove(animals[i]);
                    //Kui on viimane loom ehk dictionarys on kogus 1, siis eemalda sealt
                    //muul juhul vähenda kogust ühe võrra

                    //  animals.RemoveAt(i) - i on nagunii index
                    //  animals.RemoveAt(animals.IndexOf(animals[i])); - sama tulemus, algul leiab õige indeksi ja siis eemaldab
                    if (animalCounts[animalType] == 1) // kui count on 1, järelikult oli viimane
                    {
                        animalCounts.Remove(animalType); // remove key järgi (animal type)
                    }
                    else
                    {
                        //animalCounts[animalType] = animalCounts[animalType] - 1;
                        animalCounts[animalType]--;
                    }
                    break; //et võtaks ainult esimese lehma, mitte kõik lehmad
                }
            }
        }

       
        // 5. Täienda nii, et farmis saab olla 4 erinevat looma ja igat erinevat looma olla 5 tükki - done

        // 6. Täienda RemoveAnimal nii, et kui sellist looma ei leitud, siis prindib "looma ei leitud" -done
        // 7. Täienda GetAnimalCount(string animalType) nii, et kui sellist looma ei leitud, siis prindib "looma ei leitud"
        // 8. Looge klassid Forest ja Zoo ja mõelge, kuidas teha interface ILivingPlace, kus on kirjas kõik ühine
        // Farm, Zoo ja Forest
    }

}

