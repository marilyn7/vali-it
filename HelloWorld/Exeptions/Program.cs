﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    // Exception on programmi töös esinev erijuht
    // mille esinemisega ma peaks arvestama ja tegelema
    class Program
    {
        static void Main(string[] args)
        {
            // Try plokk eraldab koodi osa, kus me arvame, et võib juhtuda exception
            try
            {
                string word = "tere";
                word.Split(' ');
                Console.WriteLine("Sõna oli {0}", word);
                int c = 3;
                int b = 8;
                Console.WriteLine("Arvude summa on {0}", c + b);
            }
            // Püüab kinni erandi tüübist NullReferenceException 
            // ehk selline tüüp erandist, mis juhtub kui tahame kas mingit meetodit
            // või parameetrit küsida milleltki, mis on parasjagu null
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Viga word on null: " + ex.Message); ;
            }
            Console.WriteLine("Sisestage arv, millega tahate");

            int a = 5;
            try
            {
                int b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Arvude 5 ja {0} jagatis on {1}", a, b, a / b);
                int[] numbers = new int[2];
                numbers[2] = 3;
            }
            catch (DivideByZeroException ex)
            {

                Console.WriteLine("Nulliga jagada ei saa"); ;
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Sisestatud väärtus ei olnud number");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Indeks ei mahu õigesse vahesse");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Juhtus mingi muu erand");
            }
            finally
            {
                Console.WriteLine("Finaal");
            }
            Console.ReadLine();
        }
    }
}
