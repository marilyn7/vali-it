﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            //Loon klassist Monitor ühe eksemplari ehk objekti nimega monitor
            //Enum

            Monitor monitor = new Monitor();
            Monitor hpMonitor = new Monitor();
            Monitor sonyMonitor = new Monitor();

            //double diagonal = Convert.ToInt32(Console.ReadLine());
           
            monitor.Manufacturer = "Sony";
            monitor.Color = Color.Black;
            monitor.ScreenType = ScreenType.LCD;
            monitor.ScreenSize = ScreenSize.Medium;

            hpMonitor.Manufacturer = "HP";
            hpMonitor.Color = Color.Grey;
            hpMonitor.ScreenType = ScreenType.OLED;
            hpMonitor.ScreenSize = ScreenSize.Large;

            sonyMonitor.Manufacturer = "LG";
            sonyMonitor.Color = Color.White;
            sonyMonitor.ScreenType = ScreenType.TFT;
            sonyMonitor.ScreenSize = ScreenSize.Small;


            Console.WriteLine("Monitori tootja on {0}, värv {1}, diagonaal {2}, ekraani tüüp {3} ja ekraani suurus {4}", 
                monitor.Manufacturer, monitor.Color, monitor.Diagonal, monitor.ScreenType, monitor.ScreenSize);

            Console.WriteLine("Monitori tootja on {0}, värv {1}, diagonaal {2}, ekraani tüüp {3} ja ekraani suurus {4}",
                hpMonitor.Manufacturer, hpMonitor.Color, hpMonitor.Diagonal, hpMonitor.ScreenType, hpMonitor.ScreenSize);

            Console.WriteLine("Monitori tootja on {0}, värv {1}, diagonaal {2}, ekraani tüüp {3} ja ekraani suurus {4}",
                sonyMonitor.Manufacturer, sonyMonitor.Color, sonyMonitor.Diagonal, sonyMonitor.ScreenType, sonyMonitor.ScreenSize);

            // Keela kasutajal diagonaali seadistamise, luba ainult küsida
            // Kui kasutaja seadistab ekraani suuruse, siis seadista automaatselt selle järgi ka diagonaal

            int diagonal = (int)ScreenSize.Large;

            ScreenSize screenSize = (ScreenSize)65;
            Console.WriteLine(screenSize);

            Monitor[] monitors = new Monitor[3];
            monitors[0] = monitor;
            monitors[1] = hpMonitor;
            monitors[2] = sonyMonitor;

            // prindi kõigi monitoride värvid
            //1)
            for (int i = 0; i < monitors.Length; i++)
            {
                Console.WriteLine("värv on {0}", monitors[i].Color);
            }

            //2) parem ja lühem variant
            foreach (Monitor item in monitors)
            {
                Console.WriteLine(monitor.Color);
            }

            foreach (Monitor item in monitors)
            {
                if(monitor.Diagonal > 22)
                {
                    Console.WriteLine("manufacturer on {0}", monitor.Manufacturer);
                }
            }
            Console.ReadLine();
        }
    }
}

