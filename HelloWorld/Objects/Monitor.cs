﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    enum ScreenType //ENUM on tüüp, kus saab defineerida erinevaid valikuid. Kasutatakse siis, kui valikud ei muutu programmi töötamise jooksul
        //Tegelikult salvestatakse enumid alati int'na
    {
        LCD, TFT, OLED, AMOLED //kui numbreid ette ei pane, siis algab 0st
    }

    enum Color
    {
        Blue = 1, Grey = 2, Black = 3, White = 4
    }

    enum ScreenSize
    {
        Large = 27, Medium = 22, Small = 17
    }

    class Monitor
    {
        // privaatmuutujad
        // field
        private Color color;
        // in "
        private double diagonal;
        private string manufacturer;
        private ScreenType screenType;
        private ScreenSize screenSize;

        public Color Color 
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public double Diagonal
        {
            get
            {
                return diagonal;
            }
        }
       
        //public string Manufacturer { get => manufacturer; set => manufacturer = value; } 
        //selle sain manufacturer kohal lambipirniga, encapsulate
        public string Manufacturer
        {
            get //get'is ei tohi tegelikult kunagi kasutajalt küsida sisendit (readline)
            {
                if(manufacturer == "Tootja puudub")
                {
                    Console.WriteLine("Kahjuks on sellel monitoril tootja seadistamata");
                    Console.WriteLine("Palun kirjuta tootja");
                    manufacturer = Console.ReadLine();
                }
                return manufacturer;
            }
            set
            {
                if(value == "Huawei")
                {
                    Console.WriteLine("Sellise tootja monitore meil pole");
                    manufacturer = "Tootja puudub";
                }
                else
                {
                    manufacturer = value;
                }
                //value tähendab seda väärtust, mis parasjagu panen property väärtuseks
               
            }
        }

        public ScreenType ScreenType
        {
            get
            {
                return screenType; //returnima peab väikse tähega
            }
            set
            {
                screenType = value;
            }
        }
        public ScreenSize ScreenSize
        {
            get
            {
                return screenSize; //returnima peab väikse tähega
            }
            set
            {
                diagonal = (int)value;
                screenSize = value;
            }
        }

        // kapseldamine tähendab, et klass kontrollib millistele muutujatele lubab väljaspoolt ligi pääseda
        // encapsulation
    }
}
