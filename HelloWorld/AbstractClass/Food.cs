﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
    abstract class Food
    {
        // protected laseb muutujale ligi nii klassi seest kui ka pärinevates klassides
        protected int calories = 500; //protected näitab, et on nagu private, aga pärinevad klassid saavad ka kasutada
        // Erinevus interfacega
        // public abstract läheb ette

        public abstract void GoOff();
        public virtual int GetCalories()
        {
            return calories;
        }
    }
}
