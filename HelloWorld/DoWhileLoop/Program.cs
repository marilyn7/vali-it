﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
           
            bool exitProgram = false;

            //while (exitProgram)
            //{
            //    Console.WriteLine("Tere!");
            //    Console.WriteLine("Kas soovid jätkata? j/e");
            //    if(Console.ReadLine() == "j")
            //    {
            //        exitProgram = true;
            //    }
            //}

            //Do while- tsükkel erinev tavalisest while-tsüklist selle poolest, et esimene tsükli kordus tehakse alati, olenemata kas kontrollitav
            //tingimus on tõene või vale

            //Do while: tee seni, kuni on tõene
            //teeb esimese korra nagunii ära, kontrollib while alles lõpus. Siis, kui mingi tegevus on tehtud ning tahan otsustada, kas teen seda veel

            do
            {
                Console.WriteLine("Tere!");
                Console.WriteLine("Kas soovid jätkata? j/e");
            
            } while (Console.ReadLine() == "j");
        }
    }
}
