﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "Elas";
            text = text + " metsas";
            text = text + " Mutionu";
            Console.WriteLine(text);

            Console.WriteLine();
            string word1 = "Kes";
            string word2 = "elab";
            string word3 = "metsa";
            string word4 = "sees";
            string sentence = word1 + " " + word2 + " " + word3 + " " + word4;

            Console.WriteLine(sentence);
            Console.WriteLine("{0} {1} {2} {3}?", word1, word2, word3, word4);
            //Teksti formaatimine, kus määrame kohahoidjad indexitega alates 0st ja hiljem asendame muutujatega
            //eraldades muutujad komaga samas järjekorras, mis indeksid

            string nationality = "eestlane";
            string gender = "naine";
            string location = "Tallinn";
            //Ma olen naissoost, elan Tallinnas ja muidu olen Eestlane.
            string sub = gender.Substring(0, 3);
            sentence = "Ma olen " + sub + "ssoost, " + "elan " + location + "as ja muidu olen " 
                + nationality + " ning olen hea naine.";
            Console.WriteLine(sentence);
            Console.WriteLine("Ma olen {0}ssoost, elan {1}as ja muide olen {2} ning olen hea {0}ne.", 
                sub, location, nationality);

            Console.WriteLine();

            string lause = string.Format("Ma olen {0}ssoost, elan {1}as ja muide olen {2}" +
                "ning olen hea {0}ne.", sub, location, nationality);
            //string.Format teeb meile stringi valmis ja siis prindib
            //võtab vähem mälu, kui console.writeline puhul plussidega (gender, nat jne); iga plussiga uus mälu
            //paneb ühte muutujasse, dünaamiline
            Console.WriteLine(lause);
            //string.Format sisemiselt kasutab stringide liitmiseks StringBuilderit
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(word1);
            stringBuilder.Append(" ");
            stringBuilder.Append(word2);
            stringBuilder.Append(" ");
            stringBuilder.Append(word3);
            lause = stringBuilder.ToString();

            Console.ReadLine();
        }
    }
}
