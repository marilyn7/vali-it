﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //prindi ekraanile arvud 1 kuni 5

            int i = 1;
            while (i <= 5)
            {
                Console.WriteLine(i);//kirjutab i väärtuse välja
                i++; //liidab i ühe juurde ja läheb algusesse
            }

            //küsi kasutajalt mis päev täna on
            //seni kuni ta ära arvab
            string weekDay = "neljapäev";
            string answer = "";

            while (weekDay.ToLower() != answer)
            {
                Console.WriteLine("Mis päev täna on?");
                answer = Console.ReadLine();

            }
            Console.ReadLine();
        }
    }
}
