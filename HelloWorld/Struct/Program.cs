﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct
{
    class Program
    {
        static void Main(string[] args)
        {
            Table firstTable = new Table() {Height = 100, Width = 30, NumberOfLegs = 4 };

            Table secondTable = firstTable; //reference type

            Console.WriteLine(secondTable.Height);

            secondTable.Height = 40;

            Console.WriteLine(firstTable.Height);

            int a = 3;
            int b = a;
            b = 5; // value type
            Console.WriteLine(a);
            Console.WriteLine(firstTable.Height);
            IncreasedHeight(firstTable, 15);
            Console.WriteLine(firstTable.Height);
            Console.WriteLine(secondTable.Height);

            List<Table> tables = new List<Table>();
            tables.Add(firstTable);

            Table thirdTable = tables[0];
            thirdTable.Height = 100;

            Console.WriteLine();

            Console.WriteLine(firstTable.Height);
            Console.WriteLine(secondTable.Height);

            //tables[1].Height = 70;

            Console.WriteLine();

            foreach (var table in tables)
            {
                Console.WriteLine(table.Height);
            }
            int c = 4;
            Increase(c, 2);
            Console.WriteLine(c);

            Console.WriteLine();

            Chair firstChair = new Chair() { Height = 100, NumberOfLegs = 4, Width = 50 };
            Chair secondChair = firstChair;
            Console.WriteLine(secondChair.Width);

            secondChair.Width = 70;
            Console.WriteLine(secondChair.Width);
            Console.WriteLine(firstChair.Width);

            Console.WriteLine();
            Console.WriteLine(firstChair.Width);
            IncreasedWidth(firstChair, 20);
            Console.WriteLine(secondChair.Width);
            Console.WriteLine(firstChair.Width);

            // Muutuja ei näita enam arvuti mälus kuhugi
            // pole väärtust
            firstTable = null;
            //Console.WriteLine(firstTable.Width);

            // See muutuja näitab nüüd uude kohta, sinna kus paikneb uus table
            Console.WriteLine(thirdTable.Height);
            Console.WriteLine(secondTable.Height); // see ei muutu nulliks, kuna reference on ikka eelmises kohas

            Chair thirdChair;
            

            Console.ReadLine();
        }

        static void IncreasedHeight(Table table, int howMuch)
        {
            table.Height += howMuch;
        }

        static void Increase(int a, int howMuch)
        {
            a += howMuch;
        }
        static void IncreasedWidth(Chair chair, int howMuch)
        {
            chair.Width += howMuch;
        }

    }
}
