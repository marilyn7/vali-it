﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integers
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;

            number = 3;

            Console.WriteLine(number);
            int a = 4;
            int b = -7;

            //Kaks arvu on 4 ja -7.
            Console.WriteLine("Kaks arvu on {0} ja {1}", a, b);
            //või
            string sentence = string.Format("Kaks arvu on {0} ja {1}", a, b);
            //või
            Console.WriteLine(sentence);
            Console.WriteLine("Kaks arvu on " + a + " ja " + b);
            //string + string = string
            //string + int = string
            /*stringi ja täisarvu liitmisel teisendatakse täisarv stringiks ja 
             siis liidetakse*/
            //int + int = int
            //matemaatline arvude liitmine
            //arvude 4 ja -7 summa on -3
            Console.WriteLine("Arvude " + a + " ja " + b + " summa on " + (a + b));
            //lõpus on vaja panna a + b ümber sulud, see näitab tehete järjekorda, 
            //muidu liidaks lihtsalt järjest
            string lause = string.Format("Arvude {0} ja {1} summa on {2}", a, b, a + b);
            //kohal 2 olev on tehe, vastus on 3, sulge pole vaja
            Console.WriteLine(lause);
            //arvude 4 ja -7 vahe on -11
            //Arvude 4 ja -7 korrutis on -28
            //Arvude 4 ja -7 jagatis on ...
            string lause2 = string.Format("Arvude {0} ja {1} vahe on {2}", a, b, a - b);
            Console.WriteLine(lause2);
            //või
            Console.WriteLine("Arvude " + a + " ja " + b + " vahe on " + (a - b ));

            string lause3 = string.Format("Arvude {0} ja {1} korrutis on {2}", a, b, a * b);
            Console.WriteLine(lause3);
            //või
            Console.WriteLine("Arvude " + a + " ja " + b + " korrutis on " + (a * b));

            string lause4 = string.Format("Arvude {0} ja {1} jagatis on {2}", a, b, (double)a / b);
            //double teeb 15-16 kohta
            Console.WriteLine(lause4);
            //või
            Console.WriteLine("Arvude " + a + " ja " + b + " jagatis on " + (a / (float)b));
            //float teeb 7 kohta
            //tegelik vastus on -0,5, seda ei ümardata, vaid võetakse vaid numbrid enne koma
            string lause5 = string.Format("Arvude {0} ja {1} jagatis on {2}", b, a, b / a);
            Console.WriteLine(lause5);
            //või
            Console.WriteLine("Arvude " + b + " ja " + a + " jagatis on " + (b / a));
            //tegelik vastus peaks olema -1,75, aga ta sööb kõik peale koma ära, võtab aind
            //numbrid enne koma

            //kahe täisarvu jagamisel tulemus on täisarv, kus kõik peale koma süüakse ära
            //NB! Ei toimu ümardamist

            //2147483647 + 1
            int m = 2147483647;
            int n = 1;
            Console.WriteLine(m + n);
            //üle 2 miljardi numbrid ei sobi, kuna hakkab nö uuest ringist pihta
            //selle asemel kasutan siis long'i (nt isikukood)
            //http://www.blackwasp.co.uk/CSharpNumericDataTypes.aspx
            //u -ehk Unsigned, võib olla aind positiivne, 0st edasi
            //Signed ehk pos ja neg
            //long on -9x10|18, enamus ongi int või long
            Console.ReadLine();
        }
    }
}
