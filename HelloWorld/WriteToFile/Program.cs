﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WriteToFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = new string[] {
                "tore tore tore",
                "väga hea",
                "super",
                "jah" };

            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                FileStream fileStream = new FileStream("MinuFail.txt", FileMode.Create, FileAccess.Write);
                StreamWriter streamWriter = new StreamWriter(fileStream);
                //Otsib faili MinuFail.txt kaustast, kus asub minu exe fail
                //File.Append kirjutab vanale failile juurde
                //Fail.Create loob alati uue faili (vana sisu kirjutatakse üle)
                for (int i = 0; i < 10; i++)
                {
                    streamWriter.WriteLine(lines[i]);
                    streamWriter.WriteLine("väga hea");
                }

            }
            finally
            {
                streamWriter.Close();
                fileStream.Close();
            }
            
            //File.WriteAllLines("MinuFail.txt", lines); - see teeb kõik eelnevad asjad, alates rida 26 filestream, create, write, close
            //File.WriteAllText - kirjutab üksiku rea
        }
    }
}
