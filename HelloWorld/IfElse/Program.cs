﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            

            string myName = "Marilyn";
            Console.WriteLine("Palun sisesta oma nimi:");
            string name = Console.ReadLine();

            //if (name.Equals(myName, StringComparison.InvariantCultureIgnoreCase)) - saab panna lisaparameetri juurde
            // sama mis If(name == "Kalle") - ei saa lisaparameetrit kaasa panna
            if (name.ToUpper() == myName.ToUpper()) //kõige kasutatavam

            {
                Console.WriteLine("Tere, " + name  + "! Mis su vanus on?");
                int age = Convert.ToInt32(Console.ReadLine());
                if (age >= 18)
                {
                    Console.WriteLine("Tere tulemast klubisse!");
                }
                else
                {
                    Console.WriteLine("Ei saa sisse!");
                }
                    
            }

            else
            {
                Console.WriteLine("Aga mis su perekonnanimi on?");
                string sname = Console.ReadLine();

                if (sname == "Võsu" || name == "võsu")
                {
                    Console.WriteLine("Jah, me oleme samas perekonnas");
                
                }
                
                else
                {
                    Console.WriteLine("Ei meenu kohe üldse...");
                }
            }
            Console.ReadLine();
            
        }
    }
}
