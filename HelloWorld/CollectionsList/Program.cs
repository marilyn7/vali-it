﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsList
{
    class Program
    {
        static void Main(string[] args)
        {
            //Listis on elementide arv dünaamiline, saab lisada ja kustutada elemente

            //Massiiv 5st arvust, 1, 3, 0, 5, -4
            //Kustuta massiivist esimene number
            int[] numbers = new int[5]; //kui ma ette ei anna, siis täidab ikka 5-kohalise massiivi 0dega
           
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            //Massiiv 5 sõnast "kala, "auto", "", "maja", "telefon"
            //kustuta massiivist esimene sõna
            string[] words = new string[] { "kala", "auto", "", "maja", "telefon"};

            // null on tühihulk, tühjus ehk siis mälu pole eraldatud, mäluauk:D
            words[0] = null;
            words[0] = words[1];
            words[1] = "vesi";

            for (int i = 0; i < words.Length; i++)
            {
                if(words[i] == null) //null näitab, et 
                {
                    Console.WriteLine("Tühi");
                }
                else
                {
                    Console.WriteLine(words[i]);
                }
            }
            int c; //kui talle väärtust ei anna, siis eraldatakse ikka mälu, väärtus on 0
            string word; //kui talle väärtust ei anna, ei eraldata ka mälu, sest väärtus on "null"

            Console.WriteLine();
            //list, kus näeb, mis tüüpi ühikuid sinna lisada saab
            List<int> numbersList = new List<int>() { 1, 3, 22, -2 };
            numbersList.Add(5); //lisab lõppu nr 5

            Console.WriteLine(numbersList);
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.Remove(1);
            numbersList.AddRange(new int[] { 1, 2, 3 }); //lisab 1, 2, 3
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.RemoveAt(2);//võtab indexiga 2 ehk järjekorras 3. numbri ära

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.RemoveRange(2, 2);//indexiga mis, remove kui palju
            Console.WriteLine("range");

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.Insert(1, 1); //lisa mis number, mis indexiga
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.InsertRange(4, new int[] { 7, 8, 9 });//inserdib alates index, millise massiivi
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            if(numbersList.Contains(1))
            {
                Console.WriteLine("Nimekirjas on olemas number 1");
            }

            Console.WriteLine("Numbri 8 indeks on {0} ehk see on {1}. number listis", numbersList.IndexOf(8), numbersList.IndexOf(8)+1);

            Console.WriteLine("tagurpidi:");
            numbersList.Reverse(); //pööran listi tagurpidi, eest taha

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            Console.WriteLine("sorteerib:");
            numbersList.Sort(); //default sorteerib väiksemaks suuremaks
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine(numbersList.ToString());

            List<string> wordsList = new List<string>(); //kõik töötab samamoodi
            List<double> realNumbers = new List<double>(); //kõik töötab samamoodi

            ArrayList arrayList = new ArrayList() { 3, "maja", 'c', true, 3.45};//pole tüüpi, seega ei saa kasutada eelnevaid asju
            //vana funktsioon, enam eriti ei kasutata
            arrayList.Add(2);
            arrayList.Add("tere");

            int e = -8;
            string f = "kalamees";
            bool isGood = false;

            arrayList.Insert(1, e);
            arrayList.Insert(1, f);
            arrayList.Insert(1, isGood);

            Console.WriteLine();

            for (int i = 0; i < arrayList.Count; i++)
            {
                Console.WriteLine(arrayList[i]);
            }

            //et enne kontrollida, kas minu soovitud indexiga kohal on üldse string
            if(arrayList[4] is string)
            {
                word = (string)arrayList[4];
            }

            else if (arrayList[4] is int)
            {
                int g = (int)arrayList[4];
                g++;
                Console.WriteLine(g);
            }
            word = (string)arrayList[4]; //peab teadma, et kohal 4 on string kui on ilma (string)
            //int g = (int)arrayList[4]; //ta ei oska int'iks teha, sest tegemist on stringiga
            Console.WriteLine(word.ToUpper());

            //võtsin ära indexi 4 kohal sõna "maja" ja lisasin nri 4.
            //siis hüppab ta if blokis "else if"i
            arrayList.Remove("maja");

            arrayList.Insert(4, 4);
            Console.WriteLine("lisasin 4:");

            if (arrayList[4] is string)
            {
                word = (string)arrayList[4];
            }

            else if (arrayList[4] is int)
            {
                int g = (int)arrayList[4];
                g++;
                Console.WriteLine(g);
            }


            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
