﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinMaxValue
{
    class Program
    {
        static void Main(string[] args)
        {
            //leia massiivist suurim number
            int[] numbers = new int[] { 1, 3, -12, 0, 12, 2, -17 };

            //1.)
            int max = numbers[0];
            int counter = 0;
            for (int i = 1; i < numbers.Length; i++)
            {

                if (numbers[i] > max)
                {
                    max = numbers[i];
                    counter = i + 1; //see annab reaalse järjekorranumbri, kuna indeksid algavad 0st
                }

            }
            Console.WriteLine("Suurim number on {0}, mis on järjekorras  {1}.", max, counter);
            Console.WriteLine();


            //2.)
            int maxB = int.MinValue;//siin võrdleb ka indexiga 0 numbrit
            int counterB = 0;
            for (int i = 1; i < numbers.Length; i++)
            {

                if (numbers[i] > maxB)
                {
                    maxB = numbers[i];
                    counterB = i + 1; //see annab reaalse järjekorranumbri, kuna indeksid algavad 0st
                }

            }
            Console.WriteLine("Suurim number on {0}, mis on järjekorras  {1}.", max, counter);
            Console.WriteLine();

            //3.)
            int min = int.MaxValue;//siin võrdleb ka indexiga 0 numbrit
            int counterC = 0;
            for (int i = 1; i < numbers.Length; i++)
            {

                if (numbers[i] < min)
                {
                    min = numbers[i];
                    counterC = i + 1; //see annab reaalse järjekorranumbri, kuna indeksid algavad 0st
                }

            }
            Console.WriteLine("Väikseim number on {0}, mis on järjekorras  {1}.", min, counterC);
            Console.WriteLine();

            //4.)
            //Leia suurim paaritu arv
            //if(arv % 2 != 0) 
            max = numbers[0];
            counter = 0;



            for (int i = 1; i < numbers.Length; i++)
            {

                if (numbers[i] > max && numbers[i] % 2 != 0)
                {
                    max = numbers[i];
                    counter = i + 1; 
                }
            
                Console.WriteLine("Suurim paaritu arv on {}");

                Console.WriteLine("jääk on {0}", -9 % 2);//kas 9 jagub 2ga, jäägi leidmine; kas jääk on 0 
                Console.ReadLine();
            }
            if(max == int.MinValue)
            {
                Console.WriteLine("Paarituid arve ei ole");
            }
            else
            {
                Console.WriteLine("Suurim paaritu number on {0}, mis on järjekorras .");

            }

        }
    }
}
