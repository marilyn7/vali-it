﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    interface ICanDrive
    {
        int MaxDistance { get; set; }
        void Drive();
        void StopDriving(int afterDistance = 0);
    }
}
