﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Interface on liides, mille läbi 1 komponent suhtleb teise komponendiga
// Interface määrab ära public meetodid ja propertyd, mis peavad seda kasutavas (implement) klassis olemas olema
// Sama nimega, samade parameetritega ja sama tagastusväärtusega. Sisu otsustab iga klass ise
// Interface tleb, et mõlemad bird ja plane peavad oskama lennata
namespace Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane plane = new Plane();
            plane.Fly();

            Console.WriteLine();

            Bird bird = new Bird();
            bird.Fly();
            bird.LayEggs();

            Console.WriteLine();

            plane.Drive();
            plane.StopDriving();

            Console.WriteLine();

            Car car = new Car();
            car.Drive();
            car.StopDriving(100);

            Console.WriteLine();

            // Interface'i läbi saab komponente omavahel lihtsasti asendada 
            ICanDrive drivingVehicle = car;
            drivingVehicle.Drive();
            Console.WriteLine();

            List<ICanFly> flyingObjects = new List<ICanFly>();

            flyingObjects.Add(bird);
            flyingObjects.Add(plane);
            flyingObjects.Add(new Plane());
            flyingObjects.Add(new Bird());

            foreach (var flyingObject in flyingObjects)
            {
                Console.WriteLine("peaks lendama: ");
                flyingObject.Fly(); //siin peab kindlasti olema ainsuses, isegi kui programm tahab anda mitmuses
            }

            List<IBirdOrCar> birdOrCars = new List<IBirdOrCar>(); //siin tegime tühja Interface'i ainutl kopeerimise jaoks
            //siia saab panna sisse nii car ku ibird, kuna mõlemad kasutavad seda interface'i
            birdOrCars.Add(new Bird());
            birdOrCars.Add(new Car());
            birdOrCars.Add(new Bird());
            birdOrCars.Add(new Car());
            birdOrCars.Add(new Car());
            birdOrCars.Add(new Car());

            foreach (var birdOrCar in birdOrCars)
            {
                if (birdOrCar is Car)
                {
                    ((Car)birdOrCar).Drive();
                }
                else if (birdOrCar is Bird)
                {
                    ((Bird)birdOrCar).Fly();
                }
                
            }
            Console.WriteLine();

            drivingVehicle = plane;
            drivingVehicle.Drive();


            Console.ReadLine();  
        }
    }
}
