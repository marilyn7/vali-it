﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    interface ICanFly
    {
        // Määran ära, et igas klassis, mis seda interface'i implementeerib, peab olema meetod Fly(), mis ei tagasta midagi ehk void Fly();
        void Fly();
    }
}
