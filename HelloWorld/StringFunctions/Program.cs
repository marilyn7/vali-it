﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "elas metsas mutionu";
            Console.WriteLine("Lauses on {0} sümbolit.", sentence.Length);

            //Leia esimese tühiku asukoht (indeks)
            int spaceIndex = sentence.IndexOf(" ");
            Console.WriteLine("Esimene tühik on indeksiga {0}", spaceIndex);

            //leia esimene "m"
            int mIndex = sentence.IndexOf("m");
            Console.WriteLine("Esimene m on indeksiga {0}", mIndex);

            //leia esimesest 3 sümbolist koosnev lauseosa
            string subString = sentence.Substring(0, 3);
            Console.WriteLine("Esimesed 3 sümbolit on: ", subString);


            for (int i = 0; i < sentence.Length; i++) //prindib iga süboli eri reale
            {
                Console.WriteLine(sentence[i]);
            }

            string sentenceWithSpaces = "";
            for (int i = 0; i < sentence.Length; i++)
            {
                sentenceWithSpaces += sentence[i];
                if (i != sentence.Length - 1)
                {
                    sentenceWithSpaces += " ";
                }

            }

            sentenceWithSpaces = "   " + sentenceWithSpaces + "   ";
            Console.WriteLine("\"{0}\"", sentenceWithSpaces.Trim());

            Console.WriteLine("\"{0}\"", sentenceWithSpaces);
            string[] words = new string[] { "Põdral", "maja" };
            string joinedString = string.Join(" ", words); // paneb sõnad ühte ritta, lausesse

            Console.WriteLine(joinedString);
            string[] splitWords = joinedString.Split(' ');//tekstist massiivi
            for (int i = 0; i < splitWords.Length; i++)
            {
                Console.WriteLine(splitWords[i]);
            }

            //küsi kasutajalt nimekiri arvudest nii , et ta eraldab need tühikuga
            //liida need arvud kokku ja kirjuta vastus
            //kasuta split
            //
            Console.WriteLine("Sisesta numbrid nimekirja, eraldades tühikuga: ");
            string answer = Console.ReadLine();
            string[] elements = answer.Split(' '); //siin nurksulgudes ei defineeri massiivi pikkust, see tekib ise peale kasutaja sisestust
            int sum = 0;
            for (int i = 0; i < elements.Length; i++)
            {
                Console.WriteLine(elements[i]);
                sum += Convert.ToInt32(elements[i]);
                
            }
          
            Console.WriteLine("Summa on {0}", sum);
            Console.WriteLine($"Summa on {sum}"); //teine variant

            //näide kuidas erinevalt WriteLine teha (dollarimärgiga $)
            int a = 3;
            int b = 2;

            Console.WriteLine("Arvude {0} ja {1} summa on {2}", a, b, sum);
            Console.WriteLine($"Arvude {a} ja {b} summa on {sum}");

            
            //Loo numbrite massiiv ja täida see numbritega massiivist "elements"

            int[] numbers = new int[elements.Length];
            for (int i = 0; i < elements.Length; i++)
            {
                numbers[i]  = Convert.ToInt32(elements[i]);
                
            }
            Console.WriteLine("Numbrite massiiv: " + string.Join(" ", numbers));

            

            //vihje: string joinedString = string.Join(" ", words); // paneb sõnad ühte ritta, lausesse

            string plus = string.Join(" + ", elements); //join toimib nii string kui ka numbrimassiivi puhul
            
            for (int i = 0; i < plus.Length; i++)
            {
                Console.WriteLine(plus[i]);
            }
            //prindi ekraanile järgnev rida
            //answer = "2 4 6 8" => "2+4+6+8=20"
            Console.WriteLine("1.Sisestatud numbrite summa on: " + plus + " = " + sum);
            //või
            Console.WriteLine("2.Sisestatud numbrite summa on: " + string.Join(" + ", numbers) + " = " + sum);
            //või
            Console.WriteLine("3.Sisestatud numbrite summa on: {0} = {1}", string.Join(" + ", numbers), sum);//kõige mäluefektiivsem

            Console.ReadLine();
        }
        
    }
    
}
