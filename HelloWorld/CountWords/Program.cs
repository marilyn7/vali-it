﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CountWords
{
    class Program
    {
        static void Main(string[] args)
        {
            //Küsi kasutajalt lause ja loe kokku kõik erinevad sõnad ja prindi välja mitu korda need sõnad esinevad

            //"Mina ja Pets läksime tööle ja siis koju ja siis tööle jälle"
            //Mina 1x
            //ja 3x
            //Pets 1x
            //läksime 1x
            //siis 2x
            //koju 1x
            //tööle 2x
            //jälle 1x

            Dictionary<string, int> wordCounts = new Dictionary<string, int>();

            Console.WriteLine("Palun ütle lause");

            string sentence = Console.ReadLine();

            string[] words = sentence.Split(' ');  //paneb lausest iga sõna eraldi reale

            foreach (string word in words)
            {
                if (!wordCounts.ContainsKey(word)) //kui ei olnud sellist keyd
                {
                    wordCounts.Add(word, 1);
                }
                else //kui oli selline key
                {
                    wordCounts[word]++;
                    //wordCounts[word] += 1; //liidab countile juurde
                    //wordCounts[word] = wordCounts[word] + 1;
                }
                //Console.WriteLine(word);
            }
            foreach (var item in wordCounts)
            {
                Console.WriteLine("{0,8} - {1}", item.Key, item.Value);
            }

            //string match = "ja";
            //string sentence = "Mina ja Pets läksime tööle ja siis koju ja siis tööle jälle";

            //int count = 0;
            //foreach (Match match in Regex.Matches (sentence, match))
            //{
            //    count++;
            //}
            //Console.WriteLine();
            //Console.ReadLine();

            Console.ReadLine();

            
        }
    }
}
