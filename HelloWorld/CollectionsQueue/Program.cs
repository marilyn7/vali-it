﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsQueue
{
    class Program
    {
        static void Main(string[] args)
        //Queue<T> stores the values in FIFO style (First In First Out). It keeps the order in which the values were added. 
        //It provides an Enqueue() method to add values and a Dequeue() method to retrieve values from the collection.
        {
            //1, 3, 5, -7
            //anna 1
            //3 5 -7
            //lisandub 8
            //3 5 -7 
            //anna 3
            //5 -7 8 

            //Peeter Mari Kaie Paul
            //anna 1.
            //Mari Kaie Paul
            //lisandub Joel
            //Mari Kaie Paul Joel
            //anna 1.
            //Kaie Paul Joel


            Queue<string> queue = new Queue<string>();//võtan ära using.system.Linq
            queue.Enqueue("Peeter");
            queue.Enqueue("Mari");
            queue.Enqueue("Kaie");
            queue.Enqueue("Paul");
           

            //Tsükkel mingi massiivi või muu kollektsiooni läbimiseks
            //Elemente tsükli sees muuta ei saa
            //Iga queues oleva stringi kohta tee muutuja item
            //Ehk iga tsükli korduse sees on string item võrdne järgneva elemendiga
            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }
            string currentClient = queue.Dequeue();
            Console.WriteLine("Teenindatase klienti {0}", currentClient);
            Console.WriteLine();

            foreach (string item in queue)
            {
                Console.WriteLine(item);

            }

            queue.Enqueue("Joel");
            Console.WriteLine();

            //kasutan varianti "peek", et vaadata, kes järjekorras järgmine on
            Console.WriteLine("Peek:");

            foreach (string item in queue)
            {
                Console.WriteLine(item);

            }
            string nextClient = queue.Peek();

            Console.WriteLine();

            //esimene variant Mari
            if (nextClient == "Mari")
            {
                nextClient = queue.Dequeue();
                queue.Enqueue("Mari");
                Console.WriteLine("Kui Mari on järjekorras esimene, siis uus list on:");

                foreach (string item in queue)
                {
                    Console.WriteLine(item);
                }
            }
            //teine variant Mari
            if (nextClient == "Mari")
            {
                string name = queue.Dequeue();
                queue.Enqueue(name);
            }
            Console.ReadLine();
        }
    }
}
