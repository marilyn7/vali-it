﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveFilesInFolders
{
    class Program
    {
        static void Main(string[] args)
        {
            DirSearch(@"C: \Users\admin\Documents\GitHub\vali-it\HelloWorld");
            Console.ReadLine();
        }
            static void DirSearch(string sDir)
            {
                try
                {
                    foreach (string d in Directory.GetDirectories(sDir))
                    {
                        foreach (string f in Directory.GetFiles(d))
                        {
                            Console.WriteLine(f);
                        }
                        DirSearch(d);
                    }
                }
                catch (System.Exception excpt)
                {
                    Console.WriteLine(excpt.Message);
                }
            }
        }
    }

