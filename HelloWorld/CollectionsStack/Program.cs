﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsStack
{
    class Program
    {
        static void Main(string[] args)
        //Stack<T> stores the values as LIFO (Last In First Out).
        //It provides a Push() method to add a value and Pop() & Peek() methods to retrieve values. (last-added)
        {
            //Raamatud üksteise otsas:
            // "Tõde ja Õigus" "Pipi Pikksukk" "Kroonika"
            // Lisan hunnikusse raamatu "Kalevipoeg"
            // "Tõde ja Õigus" "Pipi Pikksukk" "Kroonika" "Kalevipoeg"
            // Võtan hunnikust raamatu "Kalevipoeg"
            // "Tõde ja Õigus" "Pipi Pikksukk" "Kroonika"
            // Võtan hunnikust "Kroonika"
            // "Tõde ja Õigus" 
            // Võtan hunnikust "Pipi Pikksukk"
            // "Tõde ja Õigus" 

            Stack<string> stack = new Stack<string>();
            stack.Push("Tõde ja õigus");
            stack.Push("Pipi Pikksukk");
            stack.Push("Kroonika");

            foreach (string book in stack)
            {
                //prindib ülevaltpoolt alates
                Console.WriteLine(book);
            }

            string topBook = stack.Pop(); //võtab ülemise(viimase) välja
            Console.WriteLine($"Ülemine raamat oli {topBook}");
            Console.WriteLine();

            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }

            topBook = stack.Peek();
            Console.WriteLine($"Ülemine raamat oli {topBook}"); 
            Console.WriteLine();
            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }
            Console.ReadLine();

        }
    }
}
