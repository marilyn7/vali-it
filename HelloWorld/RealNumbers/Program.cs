﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            //Non-Integer (Floating Point) Data Types

            //float	 - Single Precision Number
            //täpsus 7 kohta
            
            float a = 3;
            float b = 9;
            Console.WriteLine(a / b);
            

            //double	- Double Precision Number - DEFAULT!
            //hoiab väga suuri numbreid, aga ainult 15/16 kohta

            double c = 13000000;
            double e = 9;
            Console.WriteLine(c / e);

            a = 130000;
            b = 230000;
            c = 130000;
            e = 230000;
            Console.WriteLine(a * b);
            Console.WriteLine(c * e);

            //decimal - Decimal Number
            //hoiab väiksemaid, aga täpsemaid numbreid (precision 28/29 kohta) - pangas

            decimal f = 13000000;
            decimal g = 9;
            Console.WriteLine(f / g);

            //Pean veenduma, kas e mahub i sisse

            decimal i = (decimal)e;
            //Pean veenduma, et ma täpsuses ei kaota
            double j = (double)i;
            //toimub teisendus, sest a mahub ilusti k sisse
            double k = a;

            float l = (float)i;
            Single m = (Single)j;
            Int32 n = 22;

            //short = Int16
            //int = Int32
            //long = int64
            //float = Single
            //double = Double
            //string = String

            //Tehetel täisarvu ja reaalarvu vahel on tulemus alati antud
            Console.WriteLine((n * l) + " " + (n * l).GetType());
            Console.WriteLine((n * j) + " " + (n * j).GetType());

            int o = 5;
            double p = o;
            byte q = 2;
            decimal r = q;
            //doublisse mahuvad kõik sisse, k.a int
            //double on üle 300 koha peale koma
            //täisarvust reaalarvuks toimub alati implicit (iseeneslik) teisenemine
            Console.ReadLine();
        }
    }
}
