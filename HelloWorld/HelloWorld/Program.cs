﻿using System; 
//Annab ligipääsu kõigile klassidele, mis asuvad System nimeruumis


namespace HelloWorld 
    //Kõik klasside nimed, mis asuvad HelloWorld nimeruumi sees 
    //kehtivad selle nimeruumi piires
{
    class Program 
     //klass nimega Program
    {   
        //Staatiline meetod
        //void->meetod ei tagasta midagi
        //Main->eriline meetod, millest programm alustab tööd
        //strings[]args -> meetodi parameetrid (argumendid)    
      
        static void Main(string[] args)
        {
            Console.WriteLine("Hei!"); 
            //kutsutakse välja Console klassi meetod nimega WriteLine()
            //"Hello World!" -> Meetodi WriteLine() parameeter ehk tekst, mida välja printida
            Console.ReadLine();
            //aken jääb lahti, ootab kasutajalt vastust ehk teksti, mis lõppeb Enteriga
            Printer.Writer.Write();
            //Kutsub välja Printer nimeruumist klassi Writer meetodi Write()
            //

           }
    }
}
namespace Writer 
    //teine nimeruum, kus võib olla sama nimega klass, nagu teistes nimeruumides
{
    class Program
    {
    }
}
namespace OtherNamespace
{
    public class Program
    {
        public static void Convert()
        {
        }
    }
}

namespace Printer
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace Database
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace File
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

