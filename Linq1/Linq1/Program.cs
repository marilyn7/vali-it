﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>()
            {
                new Student()
                {
                    FirstName = "Malle",
                    LastName = "Maasikas",
                    Age = 11
                },
                 new Student()
                {
                    FirstName = "Kalle",
                    LastName = "Kaalikas",
                    Age = 18
                },
                  new Student()
                {
                    FirstName = "Laura",
                    LastName = "Labidas",
                    Age = 18
                },
                   new Student()
                {
                    FirstName = "Tõnu",
                    LastName = "Tomat",
                    Age = 23
                }

            };
            // SELECT * FROM Student
            var allStudents = (from student in students
                               select student
                              ).ToList();

            //QUERY süntaks
            var oldStudents = (from student in students //pöördun listi poole ja teen sorteerimise (sama nagu foreach)
                               where student.Age > 14
                               orderby student.FirstName descending// siin sorteerib listi eesnime järgi
                               orderby student.Age ascending
                               select student).ToList();

            // Lambda süntaks
            // the lambda operator => 
            oldStudents = students
                .Where(x => x.Age > 10)
                .OrderBy(x => x.Age)
                .OrderByDescending(x => x.FirstName)
                .ToList();


            var youngStudents = new List<Student>();
            foreach (var student in students)
            {
                if (student.Age < 20)
                {
                    youngStudents.Add(student);
                }
            }

            //anna kõikide kasutajate nime initsiaalid

            var initials = (from student in students
                            select student.FirstName[0] + " " + student.LastName[0]
                            ).ToList();



            //täienda nii, et ainult nendel õpilastel, kellel on eesnimes 5 tähte
            var initialsfive = (from student in students
                                where student.FirstName.Length == 5
                                select student.FirstName[0] + " " + student.LastName[0]
                            ).ToList();

            initials = students
                .Where(x => x.FirstName.Length == 5)
                .Select(x => x.FirstName[0] + " " + x.LastName[0])
                .ToList();


            //täisarvude massiivist anna mulle kõik paarisarvud
            int[] numbers = new int[] { 2, 3, 4, 5, 7, 8 };
            List<int> equalNumbers = (from number in numbers
                                      where number % 2 == 0
                                      select number).ToList();

            equalNumbers = numbers.Where(x => x % 2 == 0).ToList();

            //anna õpilaste nimekiri, kus perekonnanimes kõik a, e, i on asendatud o-ga

            var studentsO = (from student in students
                             select
                             new //anonüümne tüüp
                             {
                                 
                                 FirstName = student.FirstName,
                                 LastName = student.LastName == null ? student.LastName
                             .Replace('a', 'o')
                             .Replace('i', 'o')
                             .Replace('e', 'o') : null,
                                 Age = student.Age
                             }).ToList();


            //teine variant
            //studentsO = students.Select(x => new Student()
            //{
            //    FirstName = x.FirstName,
            //    LastName = x.LastName != null ? x.LastName
            //        .Replace('a', 'o')
            //        .Replace('i', 'o')
            //        .Replace("e", "o") : null,
            //    Age = x.Age
            //}).ToList();

            //üks väärtus sõltub teisest
            int a = 4;
            int b;
            if (a < 10)
            {
                b = 1;
            }
            else
            {
                b = 2;
            }
            b = a < 10 ? 1 : 2;

            //anna mulle õpilane Kalle Kaalikas
            Student oneStudent = students
                .Where(x => x.FirstName == "Kalle" && x.LastName == "Kaalikas")
                .FirstOrDefault();

            //First ja Default vahe on see, et First annab exceptioni kui sellist tulemust ei leita
            //FirstOrDefault tagastab null või (int, double puhul 0. Booleani puhul false);
            //enamasti kasutatakse FirstOrDefault
            //kui pole seda nime siis vastus null, mitte ei lähe errorisse

            
            //maksimum vanus
            var max = students.Max(x => x.Age);

            //anna vanim õpilane
            oneStudent = students.Where(x => x.Age == students.Max(y => y.Age)).FirstOrDefault();
            oneStudent = students.OrderByDescending(x => x.Age).FirstOrDefault();

            //anna õpilaste keskmine vanus

            var averageAge = students.Average(x => x.Age);
            Console.ReadLine();
        }
    }
}
