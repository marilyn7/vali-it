﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullables
{
    class Program
    {
        static void Main(string[] args)
        {
            // Annab võimaluse eristada olukorda, kas mul lihttüübil on väärtus antud või on vaikeväärtus
            int b;
            int? a = 3;
            a = null;
            a = -4;
            a = null;
            // nt kas käis koolis või ei, kas ta ei käinudki koolis või meil lihtsalt pole seda infot
            bool? attendedSchool = false;

            attendedSchool = null;

            //if(attendedSchool != null)
            if (attendedSchool.HasValue)
            {
                //Console.WriteLine("Tema väärtus on {0}", attendedSchool);
                Console.WriteLine("Tema väärtus on {0}", attendedSchool.Value);
            }
            else
            {
                Console.WriteLine("On ilma väärtuseta");
            }
            DateTime? date = null;
        }
    }
}
