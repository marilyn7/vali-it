﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Models;

namespace AspNetCore.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<AspNetCore.Models.Student> Student { get; set; }
        public DbSet<AspNetCore.Models.LoanType> LoanType { get; set; }
        public DbSet<AspNetCore.Models.Loan> Loan { get; set; }
    }
}
