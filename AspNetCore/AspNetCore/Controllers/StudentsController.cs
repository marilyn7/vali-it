﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Data;
using AspNetCore.Models;
using System.Security.Claims;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCore.Controllers
{
    public class StudentsController : Controller
    {
        private readonly ApplicationDbContext _context;

        //context on viide
        public StudentsController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult HelloName(string name, int count)
        {
            string text = "";
            for (int i = 0; i < count; i++)
            {
                text += "Hello " + name + Environment.NewLine;
            }
            return Content(text);
        }

        //https://localhost:44393/Students/helloname?name=Marilyn&count=7 - selle aadressiga on nagu käsk,
        //et printi 7 korda Hello + valitud nimi




        // GET: Students
        public IActionResult Index()
        {

            //Kasutaja kontrollimine Id järgi:
            if (User.Identity.IsAuthenticated)
            {
                var users = _userManager.Users.Where(x => x.FirstName != "Kalle").ToList();
                users = _context.Users.Where(x => x.FirstName != "Kalle").ToList();

                ViewData["Name"] = User.Identity.Name;
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }

            //Sel hetkel, kui ma käivitan .ToList()
            //Tehakse tegelikult päring andmebaasi
            //SELECT * FROM Student
            //
            var students = _context.Student
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .ToList();
            
            //Select Age, FirstName FROM Student

            var firstName = (from student in students//küsin omakorda eelmiselt vahetulemuselt, mitte andmebaasilt
                             where student.Age < 30
                             select new Student()
                             {
                                 Age = student.Age,
                                 FirstName = student.FirstName
                                }
                        ).ToList();
            return View(students);
            
        }

        // GET: Students/Details/5
        [Authorize(Roles = "Admin,HR")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        //Bind määrab ära propertied, mis tulevad sellesse meetodisse kaasa
        public async Task<IActionResult> Create(int id, [Bind("Id,FirstName,LastName,Age")] Student student)
        {
            if (ModelState.IsValid) // see tähendab kui kõik student atribuudid on õiged (nimi on olemas ja vanus int jne)
                //kontroll kas kõik väljas valideeris
            {
                // Add meetod on uue rea lisamiseks tabelisse
                // INSERT INTO student

                _context.Add(student);
                await _context.SaveChangesAsync();
                return Json(student);
            }
            return StatusCode(500, "Error occured");
        }


        public IActionResult GetImageFile(int studentId)
        {
            var student = _context.Student.Find(studentId);
            if(student.Picture == null)
            {
                return Content("Pilti pole");
            }
            return File(student.Picture, "image/jpg");
        }

        public IActionResult SumLoans()
        {
            return Content(Convert.ToString(_context.Loan.Sum(x => x.Amount)));
        }
        // GET: Students/Edit/5

            // int? tähendab, et see on int tüüp, mis lisaks võib olla ka null
            // Nullable
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                //404 errori lehe näitamine
                return NotFound();
            }
            //Find on nagu FirstOrDefault, aga otsib ainult Primary Key järgi
            var student =  _context.Student.Find(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598
        [HttpPost]
        [ValidateAntiForgeryToken]
        //Bind määrab ära propertied, mis tulevad sellesse meetodisse kaasa
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Age")] Student student, IFormFile picture)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid) // see tähendab kui kõik student atribuudid on õiged (nimi on olemas ja vanus int jne)
                                    //kontroll kas kõik väljas valideeris
            {
                try //proovib teha
                {
                    // UPDATE Student
                    //SET FirstName = student.FirstName,
                    //      LastName = student.LastName
                    //      Age = student.Age
                    // ..................
                    // WHERE
                    //    Id = student.Id
                    // Kui primary Key student.Id = 0
                    // siis tegelikult tehakse hoopis INSERT
                    // UPDATE ei kirjutata veel andmebaasi
                    // Kirjutatakse alles siis, kui kutsutakse
                    // SaveChangesAsync()

                    //Faili üles laetud pildi salvestamine
                    //FileStream stream = new FileStream("Cat03.jpg", FileMode.Create, FileAccess.Write);
                    //picture.CopyTo(stream);
                    //stream.Close();

                    MemoryStream stream = new MemoryStream();
                    picture.CopyTo(stream);
                    student.Picture = stream.ToArray();
                    stream.Close();

                    _context.Update(student);
                    _context.SaveChanges();
                }


                catch (DbUpdateConcurrencyException) //ja kui ei õnnestu siis
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound(); // 404 error
                    }
                    else // kui juhtus exception (mingi muu kui et pole kasutajat)
                    {
                        throw; // viska uuesti seesama exception mis enne püüdsin. sama hea kui et polegi catchi teinud
                    }
                }
                // Suunab edasi kindla Actionini
                // nameof(index) annab mulle kindla stringina Index actioni nime
                // Index kohale saab panna ainult neid actioneid, mis tõesti olemas
                return RedirectToAction(nameof(Index));  
            }
            // Võta seesama student objekt (uute andmetega) ja täida
            // sama view uuesti samade andmetega
            // Lisaks tulevad siia valideerimise veateated
            return View(student);
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Student.FindAsync(id);
            _context.Student.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            var student = await _context.Student.FindAsync(id);
            _context.Student.Remove(student);
            await _context.SaveChangesAsync();
            return Json(id);
        }

        private bool StudentExists(int id)
        {
            var student = _context.Student.Where(x => x.Id == id); //leia mulle selle id'ga esimene student
            if (student == null) //kui sulge ei pane, läheb if blokki ainult esimene rida. seega soovituslik on ikka sulgudega
            //{
                return false;
            //}
            return true;

            //return _context.Student.Any(e => e.Id == id); //kas on üldse sellise id-ga
        }
    }
}
