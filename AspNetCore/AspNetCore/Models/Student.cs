﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Student
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Eesnimi on kohustuslik")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "5-10 tähte")]
        [Display(Name = "First name")]
        //need sõnumid ja pealkirjad on kogu aeg FirstNamega seotud, ka edit meetodis (kui tahan studenti editida
        //ja nt tahan nime kustutada, tuleb sama errormessage. ainult see seobki, et ta on enne meetodit [] sulgudes
        public string FirstName { set; get; }
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Range(1, 35, ErrorMessage = "Vanus peab olema 1 ja 35 vahel")]
        public int? Age { get; set; }
        [Display(Name = "Marriage status (married - true; not married  - false")]
        public bool? IsMarried { get; set; }
        [Display(Name = "Blind?")]
        public bool IsBlind { get; set; }

        public List<Loan> Loans { get; set; }

        [NotMapped] //ComponentModel.DataAnnotations.Schema;
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public byte[] Picture { get; set; }
    }
}
