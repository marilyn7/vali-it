﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{   // sealed keelab sellest klassist pärinemise
    public sealed class Singleton
    {
        private static readonly Singleton INSTANCE = new Singleton();
        // Kuna konstruktor on private, siis ei saa sellest klassist otse objekti
        private Singleton() { }

        public static Singleton Instance
        {
            get
            {
                return INSTANCE;
            }
        }
    }
}
