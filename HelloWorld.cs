﻿public class HelloWorld
{
	public static void Main() //See on 1. meetod, mis käivitatakse
	{
		//Prindime ekraanile Hello World!
		System.Console.WriteLine("1-Hello World!");
		System.Console.WriteLine("  What's up?");
		System.Console.WriteLine();
		System.Console.WriteLine("2-Hi! I am programming!");
		System.Console.WriteLine("  How about you?");
	}
}